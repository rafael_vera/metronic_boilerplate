USE [master]
GO
/****** Object:  Database [boilerplate]    Script Date: 29/11/2017 02:58:28 p. m. ******/
CREATE DATABASE [boilerplate]
GO
ALTER DATABASE [boilerplate] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [boilerplate].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [boilerplate] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [boilerplate] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [boilerplate] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [boilerplate] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [boilerplate] SET ARITHABORT OFF 
GO
ALTER DATABASE [boilerplate] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [boilerplate] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [boilerplate] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [boilerplate] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [boilerplate] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [boilerplate] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [boilerplate] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [boilerplate] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [boilerplate] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [boilerplate] SET  DISABLE_BROKER 
GO
ALTER DATABASE [boilerplate] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [boilerplate] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [boilerplate] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [boilerplate] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [boilerplate] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [boilerplate] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [boilerplate] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [boilerplate] SET RECOVERY FULL 
GO
ALTER DATABASE [boilerplate] SET  MULTI_USER 
GO
ALTER DATABASE [boilerplate] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [boilerplate] SET DB_CHAINING OFF 
GO
ALTER DATABASE [boilerplate] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [boilerplate] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [boilerplate] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'boilerplate', N'ON'
GO
USE [boilerplate]
GO
/****** Object:  Table [dbo].[menus]    Script Date: 29/11/2017 02:58:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[menus](
	[id_menu] [int] IDENTITY(1,1) NOT NULL,
	[menu] [varchar](25) NOT NULL,
	[alias] [varchar](25) NOT NULL,
	[priority] [int] NOT NULL CONSTRAINT [DF_menus_priority]  DEFAULT ((-1)),
	[type] [int] NOT NULL CONSTRAINT [DF_menus_type]  DEFAULT ((0)),
	[icon] [varchar](25) NULL,
	[root] [int] NOT NULL CONSTRAINT [DF_menus_root]  DEFAULT ((-1)),
	[router] [varchar](25) NOT NULL,
 CONSTRAINT [PK_menus] PRIMARY KEY CLUSTERED 
(
	[id_menu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[roles]    Script Date: 29/11/2017 02:58:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[roles](
	[id_rol] [int] IDENTITY(1,1) NOT NULL,
	[rol] [varchar](50) NOT NULL,
	[active] [tinyint] NOT NULL CONSTRAINT [DF_roles_active]  DEFAULT ((1)),
 CONSTRAINT [PK_roles] PRIMARY KEY CLUSTERED 
(
	[id_rol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[roles_menus]    Script Date: 29/11/2017 02:58:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[roles_menus](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_rol] [int] NOT NULL,
	[id_menu] [int] NOT NULL,
	[C] [tinyint] NOT NULL CONSTRAINT [DF_roles_menus_C]  DEFAULT ((0)),
	[R] [tinyint] NOT NULL CONSTRAINT [DF_roles_menus_R]  DEFAULT ((0)),
	[U] [tinyint] NOT NULL CONSTRAINT [DF_roles_menus_U]  DEFAULT ((0)),
	[D] [tinyint] NOT NULL CONSTRAINT [DF_roles_menus_D]  DEFAULT ((0)),
 CONSTRAINT [PK_roles_menus] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[session_tokens]    Script Date: 29/11/2017 02:58:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[session_tokens](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_tag] [uniqueidentifier] NOT NULL,
	[token] [varchar](50) NOT NULL,
	[last_login] [date] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[usuarios]    Script Date: 29/11/2017 02:58:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[usuarios](
	[user_tag] [uniqueidentifier] NOT NULL CONSTRAINT [DF_usuarios_user_tag]  DEFAULT (newid()),
	[nombre] [varchar](50) NOT NULL,
	[usuario] [varchar](15) NOT NULL,
	[correo] [varchar](50) NULL,
	[psw] [varchar](100) NOT NULL,
	[id_rol] [int] NOT NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_usuarios_active]  DEFAULT ((1)),
 CONSTRAINT [PK_usuarios] PRIMARY KEY CLUSTERED 
(
	[user_tag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[menus] ON 

INSERT [dbo].[menus] ([id_menu], [menu], [alias], [priority], [type], [icon], [root], [router]) VALUES (1, N'Usuarios', N'usuarios', 0, 2, NULL, 3, N'App')
INSERT [dbo].[menus] ([id_menu], [menu], [alias], [priority], [type], [icon], [root], [router]) VALUES (2, N'Menus', N'menus', 0, 2, NULL, 4, N'App')
INSERT [dbo].[menus] ([id_menu], [menu], [alias], [priority], [type], [icon], [root], [router]) VALUES (3, N'Usuarios y Permisos', N'usuariosypermisos', 2, 2, N'flaticon-users', -1, N'App')
INSERT [dbo].[menus] ([id_menu], [menu], [alias], [priority], [type], [icon], [root], [router]) VALUES (4, N'Developers', N'developers', 1, 2, N'fa fa-code', -1, N'App')
INSERT [dbo].[menus] ([id_menu], [menu], [alias], [priority], [type], [icon], [root], [router]) VALUES (5, N'Roles', N'roles', 1, 2, NULL, 4, N'App')
INSERT [dbo].[menus] ([id_menu], [menu], [alias], [priority], [type], [icon], [root], [router]) VALUES (51, N'Admin', N'admin', 0, 1, NULL, -1, N'')
SET IDENTITY_INSERT [dbo].[menus] OFF
SET IDENTITY_INSERT [dbo].[roles] ON 

INSERT [dbo].[roles] ([id_rol], [rol], [active]) VALUES (9, N'Developer', 1)
SET IDENTITY_INSERT [dbo].[roles] OFF
SET IDENTITY_INSERT [dbo].[roles_menus] ON 

INSERT [dbo].[roles_menus] ([id], [id_rol], [id_menu], [C], [R], [U], [D]) VALUES (11, 9, 4, 1, 1, 1, 1)
INSERT [dbo].[roles_menus] ([id], [id_rol], [id_menu], [C], [R], [U], [D]) VALUES (12, 9, 2, 1, 1, 1, 1)
INSERT [dbo].[roles_menus] ([id], [id_rol], [id_menu], [C], [R], [U], [D]) VALUES (13, 9, 3, 1, 1, 1, 1)
INSERT [dbo].[roles_menus] ([id], [id_rol], [id_menu], [C], [R], [U], [D]) VALUES (14, 9, 5, 1, 1, 1, 1)
INSERT [dbo].[roles_menus] ([id], [id_rol], [id_menu], [C], [R], [U], [D]) VALUES (15, 9, 1, 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[roles_menus] OFF
INSERT [dbo].[usuarios] ([user_tag], [nombre], [usuario], [correo], [psw], [id_rol], [active]) VALUES (N'6e427841-2ccf-4823-b1a4-20c56ae4f069', N'Frank Vera', N'FVERA', N'unclemorth@hotmail.com', N'$2a$10$XktsIZj4LgGsZxbvlEBQSebNHJjb1u3B0xgbY2hgjZmBv.C5P3ALu', 9, 1)
INSERT [dbo].[usuarios] ([user_tag], [nombre], [usuario], [correo], [psw], [id_rol], [active]) VALUES (N'e59a17f5-b943-4b0c-adad-f166a793193b', N'Rafael Vera', N'RVERA', N'rafael.vera44@outlook.com', N'$2a$10$PShHHoeKQK8mTgBgqHwCceyNZf1BiyhifSVhLrUdINgb2HJYW9d3a', 9, 1)
USE [master]
GO
ALTER DATABASE [boilerplate] SET  READ_WRITE 
GO
