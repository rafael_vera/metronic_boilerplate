<?php

header('Content-Type: application/json');
$str = file_get_contents('icons.json');
$arr = json_decode($str, true);

$searchArr = explode(' ', $_GET['query']);
$result = array();
foreach ($arr as $value) {
    $r = array();
    foreach ($searchArr as $query) {
        $r = array_merge($r, search($value['children'], $query));
    }
    if (!empty($r))
        array_push($result, array("text"=>$value['text'], "children"=>$r));
}

echo json_encode($result);

function search($array, $query)
{
    $results = array_filter(
        $array, function ($item) use ($query) {
            $i = strpos($item['text'], $query);
            $i = $i === 0 ? true : $i;
            return $i;
        }
    );
    return $results;
}

?>