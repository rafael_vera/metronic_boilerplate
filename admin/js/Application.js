"use strict;"
$(document).ready(function() {
    AppRouter
        .setApp({
            name: "Boilerplate",
            author: "CoffeeCode",
            url: "",
            year: "2017"
        })
        .setDefaultOptions({
            dependencies: {
                options: {
                    insertAfter: "#plugins_after",
                    insertBefore: "#plugins_before",
                    serie: true
                }
            },
            mustacheParams: {
                canCreate: function() {
                    var permits = AppRouter.getStateExtras();
                    return parseInt(permits.C) === 1;
                },
                canRead: function() {
                    var permits = AppRouter.getStateExtras();
                    return parseInt(permits.R) === 1;
                },
                canUpdate: function() {
                    var permits = AppRouter.getStateExtras();
                    return parseInt(permits.U) === 1;
                },
                canDelete: function() {
                    var permits = AppRouter.getStateExtras();
                    return parseInt(permits.D) === 1;
                }
            }
        })
        .setDefaultState('login')
        .setNotFound({
            templateUrl: 'admin/views/error/error_1.html',
            Module: 'Error1',
            dependencies: {
                options: {
                    insertAfter: "#plugins_after",
                    insertBefore: "#plugins_before",
                    serie: true
                },
                js: [
                    "assets/vendors/particlejs/particles.js",
                    "admin/views/error/error_1.js"
                ]
            }
        })
        .setOnStateChange(function(hash, stateParams) {
            var params = stateParams;
            var d1 = $.Deferred();
            $.post('admin/views/login/main.php', { action: 'checksession' }, function(e) {
                if (e.data) {
                    mApp.scrollTop();
                    Common.activeMenu();
                    var notAllowed = params != undefined && params.params != undefined && params.params.requirePermit != undefined ? params.params.requirePermit : true;
                    permits = e.r.permits;
                    if (notAllowed && params != undefined)
                        for (let index = 0; index < permits.length; index++) {
                            if (permits[index]['alias'] == params.state) {
                                AppRouter.setStateExtras(permits[index]);
                                notAllowed = false;
                                break;
                            }
                        }
                    if (hash.replace(/^\/+|\/+$/g, '') === "login")
                        AppRouter.goToState(e.r.state[0].state);
                    else
                        $("body").removeClass('hide');
                    AppRouter.setSessionData(e.r);
                    if (notAllowed && params != undefined)
                        Common.warning("Aviso!", "Acceso denegado.")
                    d1.resolve(!notAllowed || params == undefined)
                } else {
                    $("body").addClass('hide');
                    if (params != undefined && params.state.replace(/^\/+|\/+$/g, '') !== "login")
                        AppRouter.goToState('login');
                    else
                        $("body").removeClass('hide');
                    d1.resolve(true)
                }
            });
            return d1;
        })
        .state('login', {
            url: '/Login',
            params: {
                requirePermit: false
            },
            title: 'Iniciar Sesión',
            templateUrl: 'admin/views/login/index.html',
            Module: 'Login',
            dependencies: {
                js: [
                    "assets/vendors/particlejs/particles.js",
                    "admin/views/login/main.js"
                ],
                css: [
                    "admin/views/login/style.css"
                ]
            }
        })
        .state('app', {
            url: '/App',
            title: 'App',
            params: {
                requirePermit: false
            },
            templateUrl: 'admin/views/app/index.html',
            Module: 'App',
            dependencies: {
                js: [
                    "admin/views/app/main.js"
                ]
            }
        })
        .state('app.menus', {
            url: '/Menus',
            title: 'Menus',
            templateUrl: 'admin/views/menus/index.html',
            Module: 'Menus',
            dependencies: {
                js: [
                    "assets/vendors/nested-sortable/jquery.mjs.nestedSortable.js",
                    "assets/vendors/select2/select2.full.min.js",
                    "assets/vendors/select2/i18n/es.js",
                    "admin/views/menus/main.js"
                ],
                css: [
                    "admin/views/menus/style.css"
                ]
            }
        })
        .state('app.roles', {
            url: '/Roles',
            title: 'Roles',
            templateUrl: 'admin/views/roles/index.html',
            Module: 'Roles',
            dependencies: {
                js: [
                    "admin/views/roles/main.js"
                ],
                css: [
                    "admin/views/roles/style.css"
                ]
            }
        })
        .state('app.usuarios', {
            url: '/Usuarios',
            title: 'Usuarios',
            templateUrl: 'admin/views/usuarios/index.html',
            Module: 'Usuarios',
            dependencies: {
                js: [
                    "assets/vendors/select2/select2.full.min.js",
                    "assets/vendors/select2/i18n/es.js",
                    "admin/views/usuarios/main.js"
                ]
            }
        });
    AppRouter.init();
});