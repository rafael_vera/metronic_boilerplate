"use strict;"
$.fn.extend({
    animateCss: function(animationName, callback) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
            if (callback) {
                callback();
            }
        });
        return this;
    }
});
jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es requerido.",
    email: "Por favor ingrese un correo valido.",
    url: "Por favor ingrese un URL valido.",
    date: "Por favor ingrese una fecha valida.",
    number: "Por favor ingrese un numero valido.",
    digits: "Solo se permiten numeros en este campo.",
    equalTo: "Los valores ingresados no coinciden.",
    maxlength: jQuery.validator.format("Por favor ingrese menos de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor ingrese al menos {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor ingrese un valor de entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor ingrese un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor ingrese un valor igual o menos a {0}."),
    min: jQuery.validator.format("Por favor ingrese un valor igual o mayor a {0}.")
});
var Common = function() {
    /**
     * This function set which menu element is active depending on the actual url and href of each 'a' element
     * 
     */
    function activeMenu() {
        var hash = window.location.hash;
        $('li.m-menu__item--active').removeClass('m-menu__item--active');
        var filtered = $("li.m-menu__item a").filter(function() {
            var href = $(this).attr('href').toLowerCase();
            return href.indexOf(hash.toLowerCase()) > -1 && href.length - hash.length < 3;
        });
        filtered.closest('li').addClass('m-menu__item--active');
        filtered.parents('.m-menu__submenu').each(function() {
            if (!$(this).parent('li').hasClass('m-menu__item--open'))
                $(this).siblings('.m-menu__toggle').trigger('click')
        });
    }

    function isPromise(value) {
        if (value === undefined || (typeof value === 'object' && typeof value.then !== "function")) {
            return false;
        }
        var promiseThenSrc = String($.Deferred().then);
        var valueThenSrc = String(value.then);
        return promiseThenSrc === valueThenSrc;
    }
    /**
     * Creates and shows a modal, 
     * returns a promise as response
     * 
     * @param {Object} el The element where the modal will be appended
     * @param {string} url The view url
     * @param {Object|null} options Mustache parameters
     * @returns {function} 
     */
    function modal(el, url, options) {
        Common.blockPage();
        el.off();
        options = $.extend(true, {
            options: { closeBtn: true, formId: 'modalForm' }
        }, options);
        return $.when($.get('admin/partials/modal_partial.html' + cacheBuster()), $.get(url + cacheBuster()))
            .then(function(template, partial) {
                var rendered = Mustache.render(template[0], options, { content: partial[0] });
                el.html(rendered);
                Common.unBlockPage();
                if (options.footer != undefined && options.footer.buttons != undefined) {
                    var buttons = options.footer.buttons;
                    for (let i = 0; i < buttons.length; i++) {
                        if (buttons[i].callback == undefined)
                            continue;
                        if (buttons[i].class == undefined) {
                            console.warning('Class option must be set to use a callback.');
                            continue;
                        }
                        var cls = '.' + buttons[i].class.replace(/\s+/g, '.');
                        el.find('.modal-footer').find(cls).on('click', buttons[i].callback);
                    }
                }
                el.modal();
            });
    }
    /**
     * Creates a $.post promise with the given parameters
     * 
     * @param {string} url Url of the backend webservice
     * @param {Object|boolean} params Parameters of the request or 
     * @param {Function} callback callback to be called when the request resolves
     * @returns {Promise}
     */
    function promise(url, params, callback) {
        if ($.type(params) === "function" || (params === undefined && callback === undefined))
            return $.get(url, params);
        else if (typeof params === "object")
            return $.post(url, params, callback);
        else
            console.warning("Invalid Arguments");
    }
    /**
     * Takes promises as arguments and returns a promise that resolves
     * when every request in the arguments is resolved
     * 
     * @param {Promise} args 
     * @returns {Promise}
     */
    function synchronize(...args) {
        return $.when(...args);
    }
    /**
     * Creates a Jquery.validate element with the given rules
     * 
     * @param {Object} form Form element
     * @param {Object} rules Json object with the Jquery.validate rules https://jqueryvalidation.org/rules/
     * @param {Function} valid a function that MUST return a promise, will be called  on submit
     * @param {function} reset a function that will be called after the valid function
     * @param {Object} customMessages Json object with required custom messages for the fields
     */
    function Validator(form, rules, valid, reset, customMessages) {
        form.validate({
            rules: rules,
            messages: customMessages != undefined ? customMessages : {},
            submitHandler: function(form) {
                blockElement('#' + $(form).attr('id') + ' [type="submit"]');
                $(form).find('[type="submit"]').prop('disabled', true);
                var formData = $(form).serializeObject()
                if (valid != undefined) {
                    var p = valid(formData, form);
                    if (isPromise(p))
                        p.then(function() {
                            unblockElement('#' + $(form).attr('id') + ' [type="submit"]');
                            $(form).find('[type="submit"]').prop('disabled', false);
                        });
                    else {
                        console.warn("Validator callback is not a promise");
                        setTimeout(function() {
                            unblockElement('#' + $(form).attr('id') + ' [type="submit"]');
                            $(form).find('[type="submit"]').prop('disabled', false);
                        }, 1500);
                    }
                } else
                    setTimeout(function() {
                        unblockElement('#' + $(form).attr('id') + ' [type="submit"]');
                        $(form).find('[type="submit"]').prop('disabled', false);
                    }, 1500);
                if (reset != false) {
                    form.reset();
                    $(".select2-hidden-accessible").each(function(e, v) {
                        $(v).empty();
                    });
                }
                if (reset != false && reset != undefined)
                    reset();
            },
            errorElement: "div",
            errorClass: "form-control-feedback",
            errorPlacement: function(error, element) {
                var cont = $(element).parents('.m-radio-inline, .m-select2');
                if (cont.length > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-danger');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-danger');
            },
            success: function(element) {
                element.closest('.form-group').removeClass('has-danger');
                element.remove();
            }
        });

        form.find('button[type="reset"]').on('click', function() {
            form[0].reset();
            form.validate().resetForm();
            $(".select2-hidden-accessible").each(function(e, v) {
                $(v).empty();
            });
            if (reset != undefined)
                reset();
        });
    }
    /**
     * 
     * 
     * @param {any} element Element to unblock
     */
    function blockElement(element) {
        mApp.block(element, {
            overlayColor: '#000000',
            type: 'loader',
            state: 'success',
            size: 'lg'
        });
    }
    /**
     * 
     * 
     * @param {any} element Element to block
     */
    function unblockElement(element) {
        mApp.unblock(element);
    }
    /**
     * Shows a notify notifcation
     * 
     * @param {string} title Title of the notification
     * @param {string} message Message to display
     * @param {string} type Notification type {danger, info, warning, success}
     */
    function ShowNotification(title, message, type) {
        var content = {
            message: message,
            title: title
        };
        type = type || "info";
        $.notify(content, {
            type: type,
            allow_dismiss: true,
            newest_on_top: true,
            mouse_over: true,
            spacing: 10,
            timer: 2000,
            placement: {
                from: 'top',
                align: 'right'
            },
            offset: {
                x: 30,
                y: 30
            },
            delay: 500,
            z_index: 10000,
            animate: {
                enter: 'animated fadeInDownBig',
                exit: 'animated slideOutRight'
            }
        });
    }

    function tableTemplate(table, searchInput, buttons, deferredTable) {
        if (searchInput === undefined) {
            var inputTemplate = '\
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">\
                <div class="row align-items-center">\
                    <div class="col-md-8"></div>\
                    <div class="col-md-4">\
                        <div class="m-input-icon m-input-icon--left">\
                            <input type="text" class="form-control m-input" placeholder="Busqueda..." id="searchInput">\
                            <span class="m-input-icon__icon m-input-icon__icon--left">\
                                <span>\
                                    <i class="la la-search"></i>\
                                </span>\
                            </span>\
                        </div>\
                    </div>\
                </div>\
            </div>';
            if (table.length > 0)
                table.before(inputTemplate);
        }
        if (buttons) {
            var button;
            deferredTable.then(function(dtable) {
                if (buttons.excel) {
                    button = $('\
                    <li class="m-portlet__nav-item">\
                        <a href="javascript:void(0)" title="Excel" class="m-portlet__nav-link btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill">\
                            <i class="la la-file-excel-o"></i>\
                        </a>\
                    </li>');
                    button.children().on('click', function() {
                        Print('xls', dtable);
                    });
                    table.closest('.m-portlet').children('.m-portlet__head').find('.m-portlet__nav').append(button);
                }
                if (buttons.pdf) {
                    button = $('\
                    <li class="m-portlet__nav-item">\
                        <a href="javascript:void(0)" title="PDF" class="m-portlet__nav-link btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">\
                            <i class="la la-file-pdf-o"></i>\
                        </a>\
                    </li>');
                    button.children().on('click', function() {
                        Print('pdf', dtable);
                    });
                    table.closest('.m-portlet').children('.m-portlet__head').find('.m-portlet__nav').append(button);
                }
                table.closest('.m-portlet').children('.m-portlet__head').find('.m-portlet__nav').find('a[title]').tooltip();
            });
        }

        var options = {
            layout: {
                theme: 'default',
                class: 'm-datatable--brand',
                scroll: true,
                height: null,
                footer: false,
                header: true,
                smoothScroll: {
                    scrollbarShown: true
                },
                spinner: {
                    overlayColor: '#000000',
                    opacity: 0,
                    type: 'loader',
                    state: 'brand',
                    message: true
                },
                icons: {
                    sort: { asc: 'la la-arrow-up', desc: 'la la-arrow-down' },
                    pagination: {
                        next: 'la la-angle-right',
                        prev: 'la la-angle-left',
                        first: 'la la-angle-double-left',
                        last: 'la la-angle-double-right',
                        more: 'la la-ellipsis-h'
                    },
                    rowDetail: { expand: 'fa fa-caret-down', collapse: 'fa fa-caret-right' }
                }
            },
            sortable: true,
            pagination: true,
            toolbar: {
                layout: ['pagination', 'info'],
                placement: ['bottom'],
                items: {
                    pagination: {
                        type: 'default',
                        pages: {
                            desktop: {
                                layout: 'default',
                                pagesNumber: 6
                            },
                            tablet: {
                                layout: 'default',
                                pagesNumber: 3
                            },
                            mobile: {
                                layout: 'compact'
                            }
                        },
                        navigation: {
                            prev: true,
                            next: true,
                            first: true,
                            last: true
                        },
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    },

                    info: true
                }
            },
            translate: {
                records: {
                    processing: 'Por favor espere...',
                    noRecords: 'No se encontraron registros'
                },
                toolbar: {
                    pagination: {
                        items: {
                            default: {
                                first: 'Primera',
                                prev: 'Anterior',
                                next: 'Siguiente',
                                last: 'Última',
                                more: 'Más',
                                input: 'Numero de página',
                                select: 'Seleccione tamaño de pagina'
                            },
                            info: 'Mostrando {{start}} - {{end}} de {{total}} Registros'
                        }
                    }
                }
            }
        }

        return options;
    }

    function initializeTable(table, options, searchInput, callback) {
        if (table.length > 0) {
            var datatable = table.mDatatable(options);
            var query = datatable.getDataSourceQuery() || { generalSearch: '' };
            searchInput = searchInput || $("#searchInput");
            searchInput.on('keyup', function(e) {
                var query = datatable.getDataSourceQuery() || { generalSearch: '' };
                var change = query.generalSearch != $(this).val().toLowerCase();
                query.generalSearch = $(this).val().toLowerCase();
                datatable.setDataSourceQuery(query);
                if (change)
                    datatable.load();
            }).val(query.generalSearch);
            datatable.on('m-datatable--on-layout-updated', function() {
                if (callback != undefined) {
                    callback();
                }
            });

            return datatable;
        }
    }
    /**
     * 
     * 
     * @param {any} table 
     * @param {any} cols 
     * @param {any} data 
     * @param {any} options
     * @returns 
     */
    function LocalTable(table, options) {
        var opts = options || {};
        var deferredTable = $.Deferred();
        var options = tableTemplate(table, opts.searchInput, opts.buttons, deferredTable);
        options.data = {
            type: 'local',
            source: opts.data,
            pageSize: 10
        };
        options.columns = opts.columns;
        options.detail = opts.detail;
        var datatable = initializeTable(table, options, opts.searchInput, opts.callback);
        datatable.updateData = function(data) {
            this.fullJsonData = data;
            this.reload();
        }
        deferredTable.resolve(datatable);
        return datatable;
    }
    /**
     * 
     * 
     * @param {any} table 
     * @param {any} url 
     * @param {any} options 
     * @returns 
     */
    function RemoteTable(table, options) {
        var opts = options || {};
        var deferredTable = $.Deferred();
        var options = tableTemplate(table, opts.searchInput, opts.buttons, deferredTable);
        options.data = {
            type: 'remote',
            method: 'POST',
            source: {
                read: {
                    url: opts.url,
                    params: { query: opts.params }
                }
            },
            pageSize: 10,
            saveState: {
                cookie: true,
                webstorage: true
            },
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        };
        options.columns = opts.columns;
        options.detail = opts.detail;
        var datatable = initializeTable(table, options, opts.searchInput, opts.callback);
        datatable.getColumns = function() {
            var arr = [];
            $.each(opts.columns, function(i, v) {
                if (v.field != undefined && v.field != null && v.field != '' && v.print != false) {
                    delete v.template;
                    delete v.width;
                    arr.push(v);
                }
            });
            return arr;
        };
        datatable.getReportTitle = function() {
            return opts.reportConfig.title;
        }
        datatable.getReportPageSettings = function() {
            return { size: opts.reportConfig.size || 'letter', orientation: opts.reportConfig.orientation || 'landscape', template: opts.reportConfig.template || 'genericDT' };
        }
        deferredTable.resolve(datatable);
        return datatable;
    }

    function Print(action, datatable) {
        var params = {
            dataType: 'text',
            method: 'GET',
            data: {},
            timeout: 30000,
        };
        params.url = datatable.getOption('data.source.read.url');
        if (typeof params.url !== 'string') params.url = API.getOption(
            'data.source.read');
        if (typeof params.url !== 'string') params.url = API.getOption(
            'data.source');
        params.headers = datatable.getOption('data.source.read.headers');
        params.data['datatable'] = datatable.getDataSourceParam();
        params.data['datatable'].query.action = action;
        params.data['datatable'].config = { columns: datatable.getColumns(), title: datatable.getReportTitle(), reportConfig: datatable.getReportPageSettings() };
        params.method = datatable.getOption('data.source.read.method') || 'POST';
        if (!datatable.getOption('data.serverPaging')) {
            delete params.data['datatable']['pagination'];
        }
        if (!datatable.getOption('data.serverSorting')) {
            delete params.data['datatable']['sort'];
        }
        params.success = function(r) {
            var options = {
                base64: r,
                title: 'Visor de Reportes',
                options: {
                    size: 'xl'
                },
                footer: {
                    buttons: [{
                        title: 'Cerrar',
                        class: 'btn-secondary',
                        dismiss: true
                    }]
                }
            };
            Common.unBlockPage();
            Common.modal($("#modal"), 'admin/views/modals/pdfviewer.html', options);
        }
        Common.blockPage();
        $.ajax(params);
    }

    /**
     * Use this to create a cacheBuster string for url´s
     * 
     * @returns {String}
     */
    function cacheBuster() {
        var d = new Date();
        d = '?v=' + d.getTime();
        return d;
    }
    /**
     * Default row buttons for detail, edit, delete and active/deactivate
     * 
     * Access the button with selector [data-action="type"] where type can be detail/edit/deactivate/activate/delete
     * 
     * @param {string} id Identificator for the operations of the buttons
     * @param {Object} controls
     * @param {boolean} controls.ver
     * @param {boolean} controls.editar
     * @param {boolean} controls.eliminar
     * @param {boolean} controls.activar
     * @param {boolean} controls.desactivar
     * @param {string} controls.verlbl
     * @param {string} controls.editarlbl
     * @param {string} controls.eliminarlbl
     * @param {string} controls.activarlbl
     * @param {string} controls.desactivarlbl
     * @returns {String}
     */
    function Control(id, controls) {
        var values = { ver: false, verlbl: 'Detalle', editar: false, editarlbl: 'Editar', eliminar: false, eliminarlbl: 'Eliminar', desactivar: false, desactivarlbl: 'Desactivar', activar: false, activarlbl: 'Activar' };
        $.extend(values, controls != undefined ? controls : {});
        var ret = "";
        if (values.ver)
            ret += '{{#canRead}}<a href="javascript:void(0);" data-action="detail" data-id="' + id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="' + values.verlbl + '">\
                        <i class="la la-eye"></i>\
                    </a>{{/canRead}}';
        if (values.editar)
            ret += '{{#canUpdate}}<a href="javascript:void(0);" data-action="edit" data-id="' + id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="' + values.editarlbl + '">\
                <i class="la la-edit"></i>\
            </a>{{/canUpdate}}';
        if (values.desactivar)
            ret += '{{#canUpdate}}<a href="javascript:void(0);" data-action="deactivate" data-id="' + id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="' + values.desactivarlbl + '">\
                <i class="la la-ban"></i>\
            </a>{{/canUpdate}}';
        if (values.activar)
            ret += '{{#canUpdate}}<a href="javascript:void(0);" data-action="activate" data-id="' + id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="' + values.activarlbl + '">\
                <i class="la la-check"></i>\
            </a>{{/canUpdate}}';
        if (values.eliminar)
            ret += '{{#canDelete}}<a href = "javascript:void(0);" data-action="delete" data-id="' + id + '" class = "m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"title = "' + values.eliminarlbl + '">\
                <i class = "la la-times" ></i>\
            </a>{{/canDelete}}';
        var defaults = AppRouter.getDefaultOptions();
        var stateParams = AppRouter.getStateParams();
        cachedControl = ret = Mustache.render(ret, defaults.mustacheParams);
        return ret;
    }

    function toggleAffix(affixElement, scrollElement, wrapper) {

        var height = affixElement.outerHeight(),
            top = wrapper.offset().top;

        if (scrollElement.scrollTop() >= top) {
            wrapper.height(height);
            affixElement.addClass("affix");
        } else {
            affixElement.removeClass("affix");
            wrapper.height('auto');
        }

    }

    return {
        /**
         * Unobstructive page block/loading
         * 
         */
        blockPage: function() {
            mApp.blockPage({
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                message: 'Espere...'
            });
        },
        /**
         * Unblocks the web page
         * 
         */
        unBlockPage: function() {
            mApp.unblockPage();
        },
        initTable: RemoteTable,
        localTable: LocalTable,
        blockElement: blockElement,
        unblockElement: unblockElement,
        activeMenu: activeMenu,
        validator: Validator,
        modal: modal,
        cacheBuster: cacheBuster,
        control: Control,
        promise: promise,
        sync: synchronize,
        /**
         * Displays an error notification
         * 
         * @param {any} title Title of the notification
         * @param {any} message Message to display
         */
        error: function(title, message) {
            ShowNotification(title, message, 'danger');
        },
        /**
         * Displays a success notification
         * 
         * @param {any} title Title of the notification
         * @param {any} message Message to display
         */
        success: function(title, message) {
            ShowNotification(title, message, 'success');
        },
        /**
         * Displays an info notification
         * 
         * @param {any} title Title of the notification
         * @param {any} message Message to display
         */
        info: function(title, message) {
            ShowNotification(title, message, 'info');
        },
        /**
         * Displays a warning notification
         * 
         * @param {any} title Title of the notification
         * @param {any} message Message to display
         */
        warning: function(title, message) {
            ShowNotification(title, message, 'warning');
        },
        affix: function() {
            $('[data-toggle="affix"]').each(function() {
                var ele = $(this),
                    wrapper = $('<div></div>');

                ele.before(wrapper);
                $(window).on('scroll resize', function() {
                    toggleAffix(ele, $(this), wrapper);
                });
                toggleAffix(ele, $(window), wrapper);
            });
        }
    }
}();