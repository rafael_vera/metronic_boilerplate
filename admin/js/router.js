"use strict";
var AppRouter = function() {
    var App = { name: "", author: "", year: "", url: "" };
    var DefaultOptions = {};
    var SessionData = "";
    var stateExtras = {};
    var onStateChange;
    var stateParams;
    var states = [];
    var router;
    var handlers = {};
    var depsProgress = 0;
    var depsTotal = 0;
    var insertAfter;
    var insertBefore;
    var currentLevel;
    var currentModules = [];
    const notFoundUrl = "#/notFound/#";

    function handleChanges(newHash, oldHash) {
        stateExtras = {};
        var state = getState(states, newHash.toLowerCase().split('/'));
        if (isFunction(onStateChange)) {
            onStateChange(newHash.toLowerCase(), state).then(function(e) {
                if (e)
                    goToNext(newHash, oldHash)
            });
        } else
            goToNext(newHash, oldHash)
    }

    function goToNext(newHash, oldHash) {
        var transition = router.handleURL(newHash.toLowerCase());

        if (newHash.toLowerCase().indexOf(oldHash ? oldHash.toLowerCase() : oldHash) == -1) {
            $(insertBefore).nextUntil(insertAfter).remove();
            for (var i = 0; i < currentModules.length; i++) {
                if ((currentModules[i].url !== notFoundUrl || transition.targetName != 'notFound') && newHash.toLowerCase().indexOf(currentModules[i].url.toLowerCase()) == -1) {
                    if (window[currentModules[i].module] != undefined)
                        window[currentModules[i].module] = null;
                    currentModules.splice(i, 1);
                    i--;
                }
            }
        }
    }

    function isFunction(functionToCheck) {
        var getType = {};
        return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
    }

    function getState(current, hash) {
        var state = "";
        for (const key in current) {
            if (key != "options" && current[key].options !== undefined && current[key].options.url.toLowerCase().substring(1) == hash[0]) {
                if (hash.length > 1) {
                    hash = hash.splice(1, 1);
                    state = getState(current[key], hash);
                } else {
                    state = { state: current[key].options.state, params: current[key].options.params };
                }
            }
        }

        if (state !== "")
            return state;
    }

    function pushState(pushTo, state, options) {
        if (state.length == 1) {
            if (pushTo[state[0]] == undefined)
                pushTo[state[0]] = [];
            options.state = state[0];
            pushTo[state[0]].options = options;
        } else {
            var prev = state.slice(0, 1);
            if (pushTo[prev] == undefined)
                pushTo[prev] = [];
            state.splice(0, 1);
            pushState(pushTo[prev], state, options);
        }
    }

    function goToState(current, state, url) {
        current = current[state[0]];
        if (current != undefined)
            url += current.options.url;
        else {
            console.error("Invalid state.")
            return;
        }
        if (state.length == 1) {
            window.location.hash = url;
        } else {
            state = state.splice(1, 1);
            goToState(current, state, url);
        }
    }

    function setMapping() {
        router.map(function(match) {
            match("/").to("defaultState");
            for (var key in states) {
                recursiveMapping(match, states[key], key, 0);
            }
            match("/*").to("notFound");
        });
    }

    function recursiveMapping(match, state, key, level) {
        if (Object.keys(state).length == 1) {
            match(state.options.url.toLowerCase() + '/').to(key);
            match(state.options.url.toLowerCase()).to(key);
            match("/*").to("notFound");
        } else {
            match(state.options.url.toLowerCase() + '/').to(key);
            match(state.options.url.toLowerCase()).to(key, function(match) {
                for (var key in state) {
                    if (key != "options")
                        recursiveMapping(match, state[key], key, level + 1);
                }
            });
        }
        setHandlers(key, state.options, level);
    }

    function setHandlers(key, options, level) {
        handlers[key] = {
            setup: function() {
                currentLevel = level;
                options = $.extend(true, options, DefaultOptions);
                stateParams = { title: options.title, state: options.state, params: options.params };
                if (options.Module != undefined && !hasModule(options.Module))
                    currentModules.push({ module: options.Module, url: options.url.substring(1) });
                if (options.templateUrl != undefined) {
                    var d = new Date();
                    d = '?v=' + d.getTime();
                    Common.blockPage();
                    $.get(options.templateUrl + d, function(template) {
                        var rendered = Mustache.render(template, options.mustacheParams);
                        var container = $("body").find("[content-view]");
                        container = $(container[level]);
                        container.find('*').off();
                        container.html(rendered);
                        if (container.attr('animated'))
                            container.animateCss('fadeInUp');
                        if (options.dependencies != undefined)
                            loadDependencies(options.dependencies);
                        else
                            Common.unBlockPage();
                    });
                }
            }
        };
    }

    function hasModule(module) {
        var r = false;
        $.each(currentModules, function(key, val) {
            if (val.module === module)
                r = true;
        })
        return r;
    }

    function loadDependencies(files, callback) {
        $.post('admin/config/time.php', {
            files: files
        }, function(e) {
            dependecies(e.js, 'js', files.options);
            dependecies(e.css, 'css', files.options, callback);
        });
    }

    function dependecies(bundle, type, options, callback) {
        if (options != undefined && options.insertAfter != undefined && options.insertBefore != undefined) {
            insertAfter = options.insertAfter;
            insertBefore = options.insertBefore;
        } else {
            console.error("insertAfter and insertBefore must be set.");
            return;
        }
        if (type == 'css')
            $.each(bundle, function(i, v) {
                injectStyleSheets(v);
            });
        if (type == 'js') {
            if (options.serie) {
                depsTotal++;
                loadJs(bundle[0], function() { loadNext(bundle) }, callback);
            } else
                $.each(bundle, function(i, v) {
                    depsTotal++;
                    loadJs(v.url);
                });
        }
    }

    function loadNext(bundle) {
        if (bundle.length > 1) {
            depsTotal++;
            bundle.splice(0, 1);
            loadJs(bundle[0], function() { loadNext(bundle) });
        }
    }

    function injectStyleSheets(dep) {
        var el;
        var v = dep.time ? '?v=' + dep.time : '';
        el = document.createElement('link');
        el.rel = 'stylesheet';
        el.href = dep.url + v;
        $(insertAfter).before(el);
    }
    /**
     * 
     * 
     * @param {any} dep 
     * @param {any} callback 
     */
    function loadJs(dep, callback) {
        var v = dep.time ? '?v=' + dep.time : '';
        $.ajax({
            url: dep.url + v,
            cache: true,
            dataType: "script",
            success: function() {
                depsProgress++;
                if (callback != undefined)
                    callback();
                if (depsProgress == depsTotal) {
                    if (callback !== undefined && isFunction(callback))
                        callback();
                    Common.unBlockPage();
                }
            }
        });
    }

    return {
        /**
         * Set app info
         * 
         * @param {Object} data
         * @param {string} data.name App name
         * @param {Object} data.author App creator
         * @param {string} data.year Year of creation
         * @param {string} data.url Url to the creator´s web page
         */
        setApp: function(data) {
            App = data;
            return this;
        },
        /**
         * Set default state options
         * 
         * @param {Object} options
         * @param {Object} options.dependencies
         * @param {Object} options.mustacheParams App name
         */
        setDefaultOptions: function(options) {
            DefaultOptions = options;
            return this;
        },
        /**
         * Get default state options
         * 
         */
        getDefaultOptions: function() {
            return DefaultOptions;
        },
        /**
         * Set default state
         * 
         * @param {string} state state name
         */
        setDefaultState: function(state) {
            handlers.defaultState = {
                beforeModel: function() {
                    throw "";
                    return RSVP.reject("");
                },
                events: {
                    error: function(error, transition) {
                        AppRouter.goToState(state);
                    }
                }
            };
            return this;
        },
        /**
         * Set the function that will be called every time the state changes
         * 
         * @param {function} fn function with the state change logic
         */
        setOnStateChange: function(fn) {
            onStateChange = fn;
            return this;
        },
        /**
         * Set state temporal values (resets on state change)
         * 
         * @param {Object} extras json with extra data
         */
        setStateExtras: function(extras) {
            stateExtras = extras;
        },
        /**
         * Get state temporal values (resets on state change)
         * 
         */
        getStateExtras: function() {
            return stateExtras;
        },
        /**
         * Set the not found state options
         * 
         * @param {Object} options
         * @param {string} options.url The url sub part of this state
         * @param {Object} options.params custom parameters that may be required for the state logic
         * @param {string} options.title The title the uses will see
         * @param {string} options.templateUrl Url to the template view
         * @param {string} options.Module The controller module name defined in you controller .js file
         * @param {Object} options.dependencies javascript and css dependencies of the controller 
         * @param {Object} options.mustacheParams Mustache parameters if required for the view render
         */
        setNotFound: function(options) {
            handlers.notFound = {
                setup: function(posts) {
                    var d = new Date();
                    d = '?v=' + d.getTime();
                    if (options.Module != undefined && !hasModule(options.Module))
                        currentModules.push({ module: options.Module, url: notFoundUrl });
                    $.get(options.templateUrl + d, function(template) {
                        var rendered = Mustache.render(template, options.mustacheParams);
                        var container = $("body").find("[content-view]").last();
                        container.find('*').off();
                        container.html(rendered);
                        if (container.attr('animated'))
                            container.animateCss('fadeInUp');
                        if (options.dependencies != undefined)
                            loadDependencies(options.dependencies);
                    })
                }
            };
            return this;
        },
        /**
         * Push a new state to the app
         * 
         * @param {string} state state name hierarchy is defined with '.' as separator
         * @param {Object} options
         * @param {string} options.url The url sub part of this state
         * @param {Object} options.params custom parameters that may be required for the state logic
         * @param {string} options.title The title the uses will see
         * @param {string} options.templateUrl Url to the template view
         * @param {string} options.Module The controller module name defined in you controller .js file
         * @param {Object} options.dependencies javascript and css dependencies of the controller 
         * @param {Object} options.mustacheParams Mustache parameters if required for the view render
         */
        state: function(state, options) {
            var parts = state.split('.');
            pushState(states, parts, options);
            return this;
        },
        /**
         * Go to the specified state
         * 
         * @param {string} state 
         */
        goToState: function(state) {
            if (state != 'notFound') {
                var parts = state.split('.');
                goToState(states, parts, "#");
            } else router.transitionTo(state);
        },
        /**
         * Returns the App info
         * 
         * @returns 
         */
        getApp: function() { return App },
        /**
         * Returns the accesble parameters of the current state
         * 
         */
        getStateParams: function() {
            return stateParams
        },
        /**
         * Getter for the session data that can be shared between states
         * 
         */
        getSessionData: function() {
            return SessionData;
        },
        /**
         * Setter for the session data that can be shared between states
         * 
         * @param {any} data 
         */
        setSessionData: function(data) {
            SessionData = data;
        },
        /**
         * Initialize the AppRouter, call after everything has been setup
         * 
         */
        init: function() {
            router = new Router["default"]();
            router.getHandler = function(name) {
                return handlers[name];
            };
            setMapping();
            hasher.changed.add(handleChanges);
            hasher.initialized.add(handleChanges);
            hasher.init();
        }
    }
}();