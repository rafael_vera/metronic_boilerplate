var Error1 = function() {
    return {
        init: function() {
            var d = new Date();
            d = '?v=' + d.getTime();
            particlesJS.load("particles-js", "admin/views/error/particleOptions.json" + d);
        }
    }
}();

$(document).ready(function() {
    Error1.init();
});