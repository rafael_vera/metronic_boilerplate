var App = function() {
    var d = new Date();
    var template;


    function prepareMenu() {
        var r = $.Deferred();
        Common.promise('admin/views/menus/main.php', {
            action: 'getMenusBuilder'
        }).then(function(e) {
            Common.blockElement($('#m_ver_menu > ul'));
            var t = $.Deferred();
            var p = $.Deferred();
            Common.promise('admin/partials/menu_builder.html' + d, function(template) {
                t.resolve(template);
            });
            Common.promise('admin/partials/menu_partial.html' + d, function(partial) {
                p.resolve(partial);
            });
            Common.sync(t, p).then(function(template, partial) {
                var permits = AppRouter.getSessionData();
                if (permits.permits != undefined)
                    permits = permits.permits;
                else
                    permits = [];
                var rendered = Mustache.render(template, {
                    nodes: e.r,
                    hasNodes: function() {
                        return this.nodes && this.nodes.length > 0;
                    },
                    isSection: function() {
                        return this.type == 1;
                    },
                    sectionAllowed: function() {
                        let allow = false;
                        for (let index = 0, t = false; index < e.r.length; index++) {
                            const element = e.r[index];
                            if (element.type == 1)
                                t = false;
                            if (element == this)
                                t = true;

                            if (t) {
                                for (let i = 0; i < permits.length; i++) {
                                    if (permits[i]['alias'] == element.alias) {
                                        allow = true;
                                        break;
                                    }
                                }
                                if (allow)
                                    break;
                            }
                        }
                        return allow;
                    },
                    isRoot: function() {
                        return this.root == -1;
                    },
                    urlFn: function() {
                        return '#/' + this.router + '/' + this.menu.normalize('NFD').replace(/[\u0300-\u036f]/g, "").replace(/\s/g, '');
                    },
                    hasPermit: function() {
                        var allow = false;

                        for (let index = 0; index < permits.length; index++) {
                            if (permits[index]['alias'] == this.alias) {
                                allow = true;
                                break;
                            }
                        }
                        return allow;
                    }
                }, {
                    child: partial
                });
                $('#m_ver_menu > ul').html(rendered);
                r.resolve();
                Common.unblockElement($('#m_ver_menu > ul'));
            });
        });
        return r;
    }

    function prepareHeader() {
        Common.blockElement($('header'));
        return Common.promise('admin/partials/header_partial.html' + d, function(template) {
            var rendered = Mustache.render(template, AppRouter.getSessionData());
            $('header').html(rendered);
            $('header').find('#logoutbtn').on('click', logout);
            Common.unblockElement($('header'));
        });
    }

    function prepareFooter() {
        Common.blockElement($('footer'));
        return Common.promise('admin/partials/footer_partial.html' + d, function(template) {
            var rendered = Mustache.render(template, AppRouter.getApp());
            $('footer').html(rendered);
            Common.unblockElement($('footer'));
        });
    }

    function initLayout(a, b, c) {
        mLayout.init();
        Common.activeMenu();
        hasher.changed.add(handleChanges);
        hasher.initialized.add(handleChanges);
        hasher.init();
    }

    function logout() {
        Common.promise('admin/views/login/main.php', { action: 'logout' }).then(function() {
            AppRouter.goToState('login');
        });
    }

    function handleChanges(hash) {
        setTimeout(function() {;
            var filtered = $("li.m-menu__item a").filter(function() {
                var href = $(this).attr('href').toLowerCase();
                return href.indexOf(hash.toLowerCase()) > -1 && href.length - hash.length < 3;
            });
            var raw = mLayout.menu().getBreadcrumbs(filtered.parent());
            var breadcrumbs = { title: AppRouter.getStateParams().title, breadcrumbs: [] };
            $.each(raw, function(i, v) {
                if (v.text != undefined)
                    breadcrumbs.breadcrumbs.push({ breadcrumb: v.text.trim() })
            });
            if (template != undefined) {
                renderBreadCrumbs(breadcrumbs);
                return;
            }
            Common.promise('admin/partials/breadcrumbs.html').then(function(t) {
                template = t;
                renderBreadCrumbs(breadcrumbs);
            });
        }, 200)
    }

    function renderBreadCrumbs(b) {
        var rendered = Mustache.render(template, b);
        $(".m-subheader.breadcrumbs").html(rendered);
        $("[app-name]").text(AppRouter.getApp().name + ' | ' + b.title);
    }

    function initPortlets() {
        $(".m-content").on('click', '.m-portlet__head .m-portlet__head-tools [toggle]', function() {
            $(this).children('i').toggleClass('la-angle-up').toggleClass('la-angle-down');
            $(this).closest('.m-portlet__head').siblings().slideToggle(200);
        });
    }

    return {
        init: function() {
            d = '?v=' + d.getTime();
            Common.sync(prepareMenu(), prepareFooter(), prepareHeader()).then(initLayout);
            initPortlets();
        }
    };
}();

$(document).ready(function() {
    App.init();
})