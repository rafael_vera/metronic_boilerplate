<?php

require_once '../../config/bootstrap.php';
header('Content-Type: application/json; charset=utf-8;');

Auth::getAccess("roles");

$obj = new RolesManager();
switch (true) {
    case Auth::caseAction('new') && Auth::$CAN_CREATE:
        echo $obj->newRol($request['dt']);
        break;
    case Auth::caseAction('edit') && Auth::$CAN_UPDATE:
        echo $obj->editRol($request['dt']);
        break;
    case Auth::caseAction('setActive') && Auth::$CAN_UPDATE:
        echo $obj->setActiveRol($request['id'], $request['val']);
        break;
    case Auth::caseAction('delete') && Auth::$CAN_DELETE:
        echo $obj->deleteRol($request['id']);
        break;
    case Auth::caseAction('getRol') && Auth::$CAN_UPDATE:
        echo $obj->getRol($request['id']);
        break;
    case Auth::caseAction('get') && Auth::$CAN_READ:
        echo $obj->getRoles($request);
        break;
    case Auth::caseAction('pdf') && Auth::$CAN_READ:
        $data = json_decode($obj->getRoles($request), true);
        $helpers = array(
            "permisos" => function($permsisos) {
                return implode(
                    " - ", 
                    array_map(
                        function ($entry) {
                            return $entry['name'];
                        }, $permsisos
                    )
                );
            },
            "active" => function($active) {
                return $active == 1 ? "Activo" : "Inactivo";
            }
        );
        $report = new ReportGenerator();
        $data = $report->parseData($data['data'], $request['datatable']['config'], $helpers);
        echo $report->GeneratePdf($data, $request['datatable']['config']);
        break;
    default:
        echo Auth::$DEFAULT_RESPONSE;
        break;
}