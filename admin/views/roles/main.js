"use strict;"
var Roles = function() {
    var table;

    function setEvents(el) {
        el.find("#submitForm input[type='checkbox']").on('change', function() {
            var checked = $(this).prop('checked');
            $(this).parent().siblings('div').find("input[type='checkbox']").prop('checked', checked);
            checkIndeterminate($(this));
            $('#submitForm .access-panel [name*="permisos[]"]').each(updatePermisosUI);
        });
        el.find(".access-panel i").tooltip();
        Common.promise("admin/views/modals/permisos.html").then(function(template) {
            el.find("#submitForm .access-panel").popover({
                title: "Permisos",
                html: true,
                content: template
            });
            el.find("#submitForm .access-panel").on('show.bs.popover', function() {
                el.find("#submitForm .access-panel").popover('hide');
            });
            el.find("#submitForm .access-panel").on('shown.bs.popover', function() {
                var popover = $(this);
                var id_pop = popover.attr('aria-describedby');
                popoverPermisos(popover, $("#" + id_pop), el);

            });
        });
    }

    function popoverPermisos(that, pop, el) {
        pop.on('click', '.cancel', function() {
            that.popover('hide');
        });
        that.find("input[type='hidden']").each(function(key, value) {
            pop.find("[name='menu[" + $(value).attr("id") + "]']").prop('checked', $(value).val() === "true");
        });
        pop.on('click', '.confirm', function() {
            var permisos = pop.find('form').serializeObject();
            that.find("input[type='hidden']").val(false);
            that.siblings('.m-checkbox-parent').each(function() {
                $(this).find('.access-panel').each(function() {
                    var child = $(this).siblings('label').children('input[type="checkbox"]');
                    if (child.prop('checked') || child.prop('indeterminate'))
                        $(this).find("input[type='hidden']").val(false);
                });
            });
            for (key in permisos.menu) {
                that.siblings('.m-checkbox-parent').each(function() {
                    $(this).find('.access-panel').each(function() {
                        var child = $(this).siblings('label').children('input[type="checkbox"]');
                        if (child.prop('checked') || child.prop('indeterminate'))
                            $(this).find("#" + key).val(permisos.menu[key]);
                    });
                });
                that.find("#" + key).val(permisos.menu[key]);
            }
            propagate(that, permisos);
            el.find('#submitForm .access-panel [name*="permisos[]"]').each(updatePermisosUI);
            that.popover('hide');
        });
    }

    function propagate(el, permisos) {
        var that = el.parent('.m-checkbox-parent').siblings('.access-panel');
        if (that.length > 0) {
            that.find("input[type='hidden']");
            for (key in permisos.menu) {
                that.find("#" + key).val(permisos.menu[key]);
            }
            propagate(that, permisos);
        }
    }

    function updatePermisosUI() {
        var that = $(this);
        var id = that.attr("id");
        var val = that.val() === "true" ? true : false;
        var checkbox = that.closest('.access-panel').siblings('.m-checkbox').children('[type="checkbox"]');
        var siblings = that.closest('.access-panel').siblings('.m-checkbox-parent');
        var children = siblings.find('#' + id).length;
        var checked = siblings.find('#' + id + '[value="true"]').length;

        if (!checkbox.prop('checked') && !checkbox.prop('indeterminate')) {
            that.siblings("[data-for]").removeClass('display');
            that.val(false);
            return;
        }

        if (children > 0 && checked === 0) {
            that.val(false);
            val = false;
        }

        if (val) {
            that.siblings("[data-for='NaN']").removeClass('display');
            that.siblings("[data-for='" + id + "']").addClass('display');
            if (children > 0 && checked != children)
                that.siblings("[data-for='" + id + "']").addClass('partial');
            else
                that.siblings("[data-for='" + id + "']").removeClass('partial');
        } else {
            that.siblings("[data-for='" + id + "']").removeClass('display');
            if (that.siblings("[data-for].display").not("[data-for='NaN']").length === 0)
                that.siblings("[data-for='NaN']").addClass('display');
        }
    }

    function checkIndeterminate(el) {
        var that = el.closest('.m-checkbox-parent').parent();
        var checkbox = that.children('div').find("input[type='checkbox']").length;
        var checked = that.children('div').find("input[type='checkbox']:checked").length;
        var next = that.children("label").children("input[type='checkbox']");
        if (next.length != 0) {
            next.prop('checked', checkbox === checked);
            next.prop('indeterminate', checkbox > checked && checked > 0);
            checkIndeterminate(next);
        }
    }

    function resetPemisos() {
        $(".access-panel .la").removeClass('display');
        $(".access-panel .la").not('.la-lock').removeClass('partial');
        $(".access-panel [data-for]").val(false);
    }

    function loadMenus(container, menuContainer, size, callback) {
        Common.promise('admin/views/menus/main.php', {
            action: 'getMenusBuilder'
        }).then(function(e) {
            var t = $.Deferred();
            var p = $.Deferred();
            Common.promise('admin/partials/roles_menu_base.html' + Common.cacheBuster(), function(template) {
                t.resolve(template);
            });
            Common.promise('admin/partials/roles_menu_partial.html' + Common.cacheBuster(), function(partial) {
                p.resolve(partial);
            });
            Common.sync(t, p).then(function(template, partial) {
                var i = 0;
                var rendered = Mustache.render(template, {
                    nodes: e.r,
                    hasNodes: function() {
                        return this.nodes && this.nodes.length > 0;
                    },
                    section: function() {
                        return this.type == 1;
                    },
                    size: function() {
                        return size == undefined ? 'col-lg-4' : size
                    },
                    index: function() {
                        return i++;
                    }
                }, {
                    child: partial
                });
                menuContainer.html(rendered);
                setEvents($(container));
                Common.unblockElement(container);
            }).then(callback);
        });
    }

    function getRoles() {
        Common.blockElement('.m-content');
        var options = {
            url: 'admin/views/roles/main.php',
            params: {
                action: 'get'
            },
            callback: function() {
                $("#tbl td a").tooltip();
            },
            buttons: {
                excel: true,
                pdf: true
            },
            reportConfig: {
                title: "Reporte de Roles Registrados"
            },
            columns: [{
                    field: "id_rol",
                    title: "ID",
                    width: 80,
                    textAlign: "center"
                }, {
                    field: "rol",
                    title: "Rol",
                    width: 100
                }, {
                    field: "permisos",
                    title: "Permisos",
                    template: function(row) {
                        var render = "";
                        $.each(row.permisos, function(i, v) {
                            if (i != 0)
                                render += " - ";
                            title = "";
                            for (var key in v) {
                                switch (key) {
                                    case "C":
                                        if (v[key] === "1")
                                            title += "<i class='la la-plus'></i>";
                                        break;
                                    case "R":
                                        if (v[key] === "1")
                                            title += "<i class='la la-eye'></i>";
                                        break;
                                    case "U":
                                        if (v[key] === "1")
                                            title += "<i class='la la-edit'></i>";
                                        break;
                                    case "D":
                                        if (v[key] === "1")
                                            title += "<i class='la la-times'></i>";
                                        break;
                                    default:
                                        break;
                                }
                            }
                            render += "<a href='javascript:void(0);' data-html='true' class='m-link' title=\"" + title + "\">" + v.name + "</a>";
                        });
                        return render;
                    }
                },
                {
                    field: "active",
                    title: "Status",
                    template: function(row) {
                        var status = {
                            0: { 'title': 'Inactivo', 'class': ' m-badge--danger' },
                            1: { 'title': 'Activo', 'class': ' m-badge--success' },
                        };
                        return '<span class="m-badge ' + status[row.active].class + ' m-badge--wide">' + status[row.active].title + '</span>';
                    }
                },
                {
                    field: '',
                    title: "Control",
                    template: function(row) {
                        return Common.control(row.id_rol, { ver: false, editar: true, eliminar: true, activar: row.active == "0", desactivar: row.active == "1" });
                    }
                }
            ]
        };
        table = Common.initTable($("#tbl"), options);
        $("#tbl").on('click', '[data-action="delete"]', function() {
            var that = $(this);
            var options = {
                content: 'Realmente desea eliminar este elemento?',
                title: 'Eliminar',
                footer: {
                    buttons: [{
                            title: 'Cancelar',
                            class: 'btn-secondary',
                            dismiss: true
                        },
                        {
                            title: 'Confirmar',
                            class: 'btn-primary confirm',
                            callback: function() {
                                deleteRol(that.data('id'))
                            }
                        }
                    ]
                }
            };
            Common.modal($("#modal"), 'admin/views/modals/confirm.html', options);
        });
        $("#tbl").on('click', '[data-action="deactivate"], [data-action="activate"]', function() {
            var that = $(this);
            Common.promise('admin/views/roles/main.php', {
                action: 'setActive',
                id: that.data('id'),
                val: that.data('action') == "activate" ? 1 : 0
            }, function(e) {
                if (e.data) {
                    $("#tbl td a").tooltip('dispose');
                    table.load();
                } else {
                    Common.error("Error!", e.r);
                }
            });
        });
        $("#tbl").on('click', '[data-action="edit"]', function() {
            var that = $(this);
            var options = {
                title: 'Editar',
                options: {
                    form: true,
                    size: 'lg',
                    formId: 'submitForm'
                },
                footer: {
                    buttons: [{
                            title: 'Cancelar',
                            class: 'btn-secondary',
                            dismiss: true
                        },
                        {
                            title: 'Guardar',
                            class: 'btn-primary',
                            type: 'submit'
                        }
                    ]
                }
            };
            Common.modal($("#modal"), 'admin/views/modals/edit_rol.html', options)
                .then(function() {
                    Common.promise('admin/views/roles/main.php', { action: 'getRol', id: that.data('id') })
                        .then(function(e) {
                            if (e.data) {
                                $("#modal #submitForm").find("#rol").val(e.r.rol);
                                $("#modal #submitForm").find("#state").val(e.r.state);
                                loadMenus('#modal', $('#edit_menus'), 'col-lg-6', function() {
                                    $.each(e.r.permisos, function(i, v) {
                                        var refInput = $("#modal #submitForm").find("[name='permisos[][id_menu]'][value='" + parseInt(v.id_menu) + "']");
                                        var panel = refInput.parent().siblings('.access-panel');
                                        refInput.siblings('input[type="checkbox"]').prop('checked', true);
                                        checkIndeterminate(refInput.siblings('input[type="checkbox"]'));
                                        for (const key in v) {
                                            panel.find("#" + key).val(v[key] == "1");
                                        }
                                    });
                                    $('#submitForm .access-panel [name*="permisos[]"]').each(updatePermisosUI);
                                });
                                $("#id_rol").val(that.data('id'));
                                var rules = {
                                    rol: {
                                        required: true
                                    },
                                    id_rol: {
                                        required: true
                                    }
                                };
                                Common.validator($("#modal #submitForm"), rules, editarRol);
                            } else if (e.error)
                                Common.error(e.r);
                        });
                });
        });
    }

    function nuevoRol(data) {
        return Common.promise('admin/views/roles/main.php', { action: 'new', dt: data }, function(e) {
            if (e.data) {
                Common.info("Atencion!", "Rol registrado con exito.");
                table.load();
            } else {
                Common.error("Error!", e.r);
            }
        });
    }

    function editarRol(data) {
        return Common.promise('admin/views/roles/main.php', { action: 'edit', dt: data }, function(e) {
            if (e.data) {
                Common.info("Atencion!", "Rol actualizado con exito.");
                $('#modal').modal('toggle');
                table.load();
            } else {
                Common.error("Error!", e.r);
            }
        });
    }

    function deleteRol(id) {
        Common.blockElement('#modal .modal-content');
        Common.promise('admin/views/roles/main.php', {
            action: 'delete',
            id: id
        }, function(e) {
            if (e.data) {
                Common.unblockElement('#modal .modal-content');
                $("#modal").modal('toggle');
                Common.info("Atencion!", "Rol eliminado satisfactoriamente.");
                table.load();
            } else {
                Common.error("Error!", e.r);
            }
        });
    }

    return {
        init: function() {
            var rules = {
                rol: {
                    required: true
                }
            };
            Common.validator($("#submitForm"), rules, nuevoRol, resetPemisos);
            Common.blockElement('.m-content');
            loadMenus('.m-content', $("#menus"));
            getRoles();
        }
    };
}();

$(document).ready(function() {
    Roles.init();
});