"use strict;"

var Usuarios = function() {
    var table;
    var rules = {
        usuario: {
            required: true,
            remote: {
                url: "admin/views/usuarios/main.php",
                type: "post",
                data: {
                    action: 'userExist',
                    username: function() {
                        return $("#usuario").val();
                    }
                }
            }
        },
        psw: {
            required: true
        },
        confirmar: {
            equalTo: "#psw"
        },
        nombre: {
            required: true
        },
        correo: {
            email: true
        },
        rol: {
            required: true
        }
    };
    var customMessages = {
        usuario: {
            remote: "Nombre de usuario ya registrado."
        }
    };

    function setEvents() {
        Common.validator($("#submitForm"), rules, newUsuario, null, customMessages);
    }

    function getUsuarios() {
        var options = {
            url: 'admin/views/usuarios/main.php',
            params: { action: 'get' },
            reportConfig: {
                title: "Reporte de Usuarios Registrados"
            },
            buttons: {
                excel: true,
                pdf: true
            },
            callback: function() {
                $("#tbl td a").tooltip();
            },
            columns: [{
                    field: 'nombre',
                    title: 'Nombre Completo'
                },
                {
                    field: 'usuario',
                    title: 'Usuario'
                },
                {
                    field: 'correo',
                    title: 'Correo'
                },
                {
                    field: 'rol',
                    title: 'Rol',
                    width: 80
                },
                {
                    field: "active",
                    title: "Status",
                    width: 70,
                    template: function(row) {
                        var status = {
                            0: { 'title': 'Inactivo', 'class': ' m-badge--danger' },
                            1: { 'title': 'Activo', 'class': ' m-badge--success' },
                        };
                        return '<span class="m-badge ' + status[row.active].class + ' m-badge--wide">' + status[row.active].title + '</span>';
                    }
                },
                {
                    field: '',
                    title: "Control",
                    width: 140,
                    template: function(row) {
                        return Common.control(row.user_tag, { ver: false, editar: true, eliminar: true, activar: row.active == "0", desactivar: row.active == "1" });
                    }
                }
            ]
        };
        table = Common.initTable($("#tbl"), options);
        $("#tbl").on('click', '[data-action="delete"]', deleteUsuario);
        $("#tbl").on('click', '[data-action="deactivate"], [data-action="activate"]', toggleActive);
        $("#tbl").on('click', '[data-action="edit"]', editUsuarioModal);
    }

    function getRoles(el) {
        return Common.promise('admin/views/usuarios/main.php', { action: 'getRoles' }, function(e) {
            if (e.data || !e.error) {
                el.find("#rol").select2({
                    language: "es",
                    width: '100%',
                    allowClear: true,
                    placeholder: 'Seleccione un rol',
                    data: e.r
                });
            } else if (e.error) {
                Common.error("Error!", e.r);
            }
        });
    }

    function newUsuario(data) {
        return Common.promise('admin/views/usuarios/main.php', { action: 'new', dt: data }, function(e) {
            if (e.data) {
                Common.info("Atencion!", "Usuario registrado con exito.");
                table.load();
            } else if (e.error) {
                Common.error("Error!", e.r);
            }
        });
    }

    function editUsuarioModal() {
        var that = $(this);
        var options = {
            title: 'Editar',
            options: {
                size: 'lg',
                form: true
            },
            footer: {
                buttons: [{
                    title: 'Cancelar',
                    class: 'btn-secondary',
                    dismiss: true
                }, {
                    title: 'Guardar',
                    class: 'btn-primary',
                    type: 'submit'
                }]
            }
        };
        Common.modal($("#modal"), 'admin/views/modals/edit_usuario.html', options)
            .then(function() {
                Common.promise('admin/views/usuarios/main.php', { action: 'getUsuario', id: that.data('id') }, function(e) {
                    if (e.data) {
                        var rulesCopy = $.extend({}, rules);;
                        rulesCopy.psw.required = false;
                        rulesCopy.confirmar.equalTo = "#modalForm #psw";
                        rulesCopy.confirmar.required = function() {
                            return $("#modalForm").find("#psw").val() != "";
                        };
                        Common.validator($("#modalForm"), rulesCopy, editUsuario, null, customMessages);
                        getRoles($("#modalForm")).then(function() {
                            $("#modalForm").find("#usuario").val(e.r.usuario);
                            $("#modalForm").find("#nombre").val(e.r.nombre);
                            $("#modalForm").find("#correo").val(e.r.correo);
                            $("#modalForm").find("#rol").val(e.r.id_rol).trigger('change');
                        });
                        $("#user_tag").val(that.data('id'));
                    } else if (e.error)
                        Common.error("Error", e.r);
                });

            });
    }

    function editUsuario(data) {
        return Common.promise('admin/views/usuarios/main.php', { action: 'update', dt: data }, function(e) {
            if (e.data) {
                Common.info("Aviso!", "Usuario actualizado satisfactoriamente.");
                $("#modal").modal('toggle');
                table.load();

            } else if (e.error)
                Common.error("Error!", e.r);
        });
    }

    function deleteUsuario() {
        var that = $(this);
        var options = {
            content: 'Realmente desea eliminar este elemento?',
            title: 'Eliminar',
            footer: {
                buttons: [{
                        title: 'Cancelar',
                        class: 'btn-secondary',
                        dismiss: true
                    },
                    {
                        title: 'Confirmar',
                        class: 'btn-primary confirm',
                        callback: function() {
                            delUsuario(that.data('id'))
                        }
                    }
                ]
            }
        };
        Common.modal($("#modal"), 'admin/views/modals/confirm.html', options);
    }

    function delUsuario(id) {
        Common.blockElement('#modal .modal-content');
        Common.promise('admin/views/usuarios/main.php', {
            action: 'delete',
            id: id
        }, function(e) {
            if (e.data) {
                Common.unblockElement('#modal .modal-content');
                $("#modal").modal('toggle');
                Common.info("Atencion!", "Usuario eliminado satisfactoriamente.");
                table.load();
            } else {
                Common.error("Error!", e.r);
            }
        });
    }

    function toggleActive() {
        var that = $(this);
        Common.promise('admin/views/usuarios/main.php', {
            action: 'setActive',
            id: that.data('id'),
            val: that.data('action') == "activate" ? 1 : 0
        }).then(function(e) {
            if (e.data) {
                $("#tbl td a").tooltip('dispose');
                table.load();
            } else {
                Common.error("Error!", e.r);
            }
        });

    }


    return {
        init: function() {
            setEvents();
            getUsuarios();
            getRoles($("#submitForm"));
        }
    };
}();


$(document).ready(function() {
    Usuarios.init();
});