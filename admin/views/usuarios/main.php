<?php

require_once '../../config/bootstrap.php';
header('Content-Type: application/json; charset=utf-8;');

Auth::getAccess("usuarios");

$obj = new UsuariosManager();
switch (true) {
    case Auth::caseAction('getUsuario') && Auth::$CAN_UPDATE:
        echo $obj->getUsuario($request['id']);
        break;
    case Auth::caseAction('new') && Auth::$CAN_CREATE:
        echo $obj->newUsuario($request['dt']);
        break;
    case Auth::caseAction('update') && Auth::$CAN_UPDATE:
        echo $obj->updateUsuario($request['dt']);
        break;
    case Auth::caseAction('setActive') && Auth::$CAN_UPDATE:
        echo $obj->setActiveUsuario($request['id'], $request['val']);
        break;
    case Auth::caseAction('delete') && Auth::$CAN_DELETE:
        echo $obj->deleteUsuario($request['id']);
        break;
    case Auth::caseAction('getRoles') && Auth::$CAN_UPDATE:
        $obj = new RolesManager();
        echo $obj->getRolesArray();
        break;
    case Auth::caseAction('userExist') && Auth::$CAN_UPDATE:
        echo $obj->userExist($request['username']);
        break;
    case Auth::caseAction('get') && Auth::$CAN_READ:
        echo $obj->getUsuarios($request);
        break;
    case Auth::caseAction('pdf') && Auth::$CAN_READ:
        $data = json_decode($obj->getUsuarios($request), true);
        $helpers = array(
            "active" => function($active) {
                return $active == 1 ? "Activo" : "Inactivo";
            }
        );
        $report = new ReportGenerator();
        $data = $report->parseData($data['data'], $request['datatable']['config'], $helpers);
        echo $report->GeneratePdf($data, $request['datatable']['config']);
        break;
    case Auth::caseAction('xls') && Auth::$CAN_READ:
        $data = $obj->getUsuarios($request);
        echo $data;
        break;
    default:
        echo Auth::$DEFAULT_RESPONSE;
        break;
}