var Login = function() {
    function setupParticles() {
        $("#particles-js").css('height', $('.particle-wrapper').outerHeight());
        particlesJS.load("particles-js", "admin/views/error/particleOptions2.json" + Common.cacheBuster());
    }

    function setupLogin() {
        var rules = {
            user: {
                required: true
            },
            psw: {
                required: true
            }
        };
        Common.validator($("#loginForm"), rules, LoginCallback);
    }

    function LoginCallback(data) {
        return Common.promise('admin/views/login/main.php', { action: 'login', dt: data }, function(e) {
            if (e.data) {
                AppRouter.goToState(e.r[0].state);
                Common.success("Aviso!", "Inicio sesión correctamente.");
            } else if (e.error) {
                Common.error("Error!", e.r);
            }
        });
    }

    return {
        init: function() {
            $("[app-name]").text(AppRouter.getApp().name + ' | ' + 'Iniciar Sesión');
            setupParticles();
            setupLogin();
        }
    }
}();

$(document).ready(function() {
    Login.init();
});