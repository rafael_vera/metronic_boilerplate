<?php

require_once '../../config/bootstrap.php';
header('Content-Type: application/json; charset=utf-8;');

if (isset($request['action'])) {
    session_start();
    $obj = new SessionManager();
    switch ($request['action']) {
    case 'login':
        echo $obj->login($request['dt']);
        break;
    case 'logout':
        echo $obj->logOut();
        break;
    case 'checksession':
        echo $obj->checkSession();
        break;
    default:
        echo Auth::$DEFAULT_RESPONSE;
        break;
    }

    session_write_close();
}