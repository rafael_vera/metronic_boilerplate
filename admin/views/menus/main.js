"use strict;"

var Menus = function() {
    var ol;

    function nuevo(data) {
        data.alias = aliasFn($("#submitForm #menu"));
        return Common.promise('admin/views/menus/main.php', {
            action: 'newMenu',
            dt: data
        }, function(e) {
            if (e.data) {
                prepareMenuBuilder();
                Common.info("Atencion!", "Menú creado satisfactoriamente.");
            } else {
                Common.error("Atencion!", e.r);
            }
        });
    }

    function editar(data, form) {
        data.alias = aliasFn($(form).find("#menu"));
        return Common.promise('admin/views/menus/main.php', {
            action: 'updateMenu',
            dt: data
        }, function(e) {
            if (e.data) {
                Common.info("Atencion!", "Menú modificado satisfactoriamente.");
                prepareMenuBuilder();
            } else {
                Common.error("Atencion!", e.r);
            }
        });
    }

    function eliminar() {
        var that = $(this);
        var options = {
            title: 'Eliminar',
            content: 'Realmente desea eliminar este elemento?',
            footer: {
                buttons: [{
                        title: 'Cancelar',
                        class: 'btn-secondary',
                        dismiss: true
                    },
                    {
                        title: 'Confirmar',
                        class: 'btn-primary confirm',
                        callback: function() {
                            deleteMenu(that.attr('data-id'))
                        }
                    }
                ]
            }
        };
        Common.modal($("#modal"), 'admin/views/modals/confirm.html', options);
    }

    function deleteMenu(id) {
        Common.blockElement('#modal .modal-content');
        Common.promise('admin/views/menus/main.php', {
            action: 'deleteMenu',
            id: id
        }, function(e) {
            if (e.data) {
                Common.unblockElement('#modal .modal-content');
                $("#modal").modal('toggle');
                Common.info("Atencion!", "Menú eliminado satisfactoriamente.");

            } else {
                Common.error("Error!", e.r);
            }
        }).then(prepareMenuBuilder);
    }

    function editarFn(e) {
        var target = $(e.delegateTarget).parent().siblings('form');
        $("[name*='editForm_']").not("[name*='" + target.attr('name') + "']").slideUp(300);
        $("[name*='editForm_']").not("[name*='" + target.attr('name') + "']").data('validator', null);
        $("[name*='editForm_']").not("[name*='" + target.attr('name') + "']").unbind('validate');
        target.slideToggle(300);
        $("[name*='editForm_'] [name*='icon_']").each(function() {
            if ($(this).hasClass("select2-hidden-accessible"))
                $(this).select2('destroy');
        });
        setupEvents(target, editar);
    }

    function menuChange() {
        Common.promise('admin/views/menus/main.php', {
            action: 'updateMenuBuilder',
            menu: ol.nestedSortable('toHierarchy')
        }, function(e) {
            if (e.data) {
                prepareMenuBuilder();
            } else if (e.error) {
                Common.error("Error!", e.r);
                prepareMenuBuilder();
            }
        });
    }

    function prepareMenuBuilder() {
        var d = new Date();
        d = '?v=' + d.getTime();

        Common.promise('admin/views/menus/main.php', {
            action: 'getMenusBuilder'
        }).then(function(e) {
            var t = $.Deferred();
            var p = $.Deferred();
            Common.promise('admin/partials/menu_builder.html' + d, function(template) {
                t.resolve(template);
            });
            Common.promise('admin/partials/menu_builder_partial.html' + d, function(partial) {
                p.resolve(partial);
            });
            Common.sync(t, p).then(function(template, partial) {
                var rendered = Mustache.render(template, {
                    nodes: e.r,
                    hasNodes: function() {
                        return this.nodes && this.nodes.length > 0;
                    },
                    isRoot: function() {
                        return this.root == -1;
                    },
                    isSection: function() {
                        return this.type == 1;
                    },
                    iconName: function() {
                        var iconArr = this.icon.split(' ');
                        return iconArr.length > 1 ? iconArr[iconArr.length - 1] : this.icon;
                    }
                }, {
                    child: partial
                });
                $('#menu_builder').html(rendered);
            }).then(initMenuBuilder);
        });
    }

    function initMenuBuilder() {
        ol = $('ol.sortable').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div.handle',
            helper: 'clone',
            items: 'li.item',
            opacity: .6,
            isTree: true,
            errorClass: 'placeholder-error',
            placeholder: 'placeholder',
            tabSize: 15,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 3,
            excludeRoot: true,
            expandOnHover: 700,
            startCollapsed: false,
            isAllowed: function(item, parent, sorted) {
                if (sorted.attr("data-type") != 1) {
                    if (parent != undefined && parent.attr("data-type") == 1)
                        return false;
                    return true;
                } else if (sorted.attr("data-type") == 1 && parent == undefined)
                    return true;
                return false;
            },
            relocate: menuChange
        });
        $('.m-portlet__head-tools').on('click', '.disclose', function() {
            $(this).parent().parent().parent().closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
            $(this).children('i').toggleClass('la-angle-down').toggleClass('la-angle-up');
        });
        $('.m-portlet__head-tools').on('click', '.delete', eliminar);
        $('.m-portlet__head-tools').on('click', '.edit', editarFn);
    }

    function setupEvents(form, callback) {
        form.find("#icon").select2({
            minimumInputLength: 2,
            language: "es",
            ajax: {
                url: "assets/app/json/icons.php",
                dataType: "json",
                delay: 250,
                type: "GET",
                data: function(params) {
                    var queryParameters = {
                        query: params.term
                    }
                    return queryParameters;
                },
                processResults: function(data) {
                    return {
                        results: data
                    };
                }
            },
            templateResult: format,
            templateSelection: format
        });

        form.find("#menu").on('keyup', function() {
            form.find("#alias").text(aliasFn($(this)));
            form.find("#url").text(urlFn(form.find("#router"), $(this)));
        });
        form.find("#router").on('keyup', function() {
            form.find("#url").text(urlFn($(this), form.find("#menu")));
        });

        var rules = {
            type: {
                required: true,
                range: [0, 1]
            },
            menu: {
                required: true
            },
            router: {
                required: '#type_menu:checked'
            },
            icon: {
                required: '#type_menu:checked'
            }
        };
        Common.validator(form, rules, callback)
    }

    function aliasFn(menu) {
        return menu != undefined ? menu.val().normalize('NFD').toLowerCase().replace(/[\u0300-\u036f]/g, "").replace(/\s+/g, '_') : '';
    }

    function urlFn(router, menu) {
        return menu.val() && router.val() ? '#/' + router.val() + '/' + menu.val() : '';
    }

    function format(item) {
        return $('<div><i style="margin-right: 12px; " class=" ' + item.id + ' "></i>' + item.text + '</div>');
    }

    return {
        init: function() {
            prepareMenuBuilder();
            setupEvents($("#submitForm"), nuevo);
        }
    }
}();

$(document).ready(function() {
    Menus.init();
});