<?php

require_once '../../config/bootstrap.php';
header('Content-Type: application/json; charset=utf-8;');

Auth::getAccess("menus");

$obj = new MenusManager();
switch (true) {
    case Auth::caseAction('getMenusBuilder') && Auth::$CAN_READ:
        echo $obj->getMenusBuilder();
        break;
    case Auth::caseAction('updateMenuBuilder') && Auth::$CAN_UPDATE:
        echo $obj->updateMenuPosition($request['menu']);
        break;
    case Auth::caseAction('newMenu') && Auth::$CAN_CREATE:
        echo $obj->newMenu($request['dt']);
        break;
    case Auth::caseAction('updateMenu') && Auth::$CAN_UPDATE:
        echo $obj->updateMenu($request['dt']);
        break;
    case Auth::caseAction('deleteMenu') && Auth::$CAN_DELETE:
        echo $obj->deleteMenu($request['id']);
        break;
    default:
        echo Auth::$DEFAULT_RESPONSE;
        break;
}