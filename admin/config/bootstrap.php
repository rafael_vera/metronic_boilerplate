<?php
function autoload($className) 
{
    //class directories
    $directorys = array(
        '../../classes/',
        '../../config/'
    );
    
    foreach ($directorys as $directory) {
        if (file_exists($directory.$className . '.php')) {
            include_once $directory.$className . '.php'; 
            return;
        }            
    }
}

spl_autoload_register('autoload');

switch($_SERVER['REQUEST_METHOD'])
{
    case 'GET': 
        $request = $_GET;
        break;
    case 'POST': 
        $request = $_POST; 
        break;
}

if (isset($request["datatable"]) && isset($request["datatable"]['query']["action"])) {
    $request['action'] = $request["datatable"]['query']["action"];
    unset($request["datatable"]['query']["action"]);
}

Auth::$REQUEST = $request;