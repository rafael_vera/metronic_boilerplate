<?php

abstract class ConnectionManager extends DataTable
{
    //private $server="DESKTOP-C6156C8";
    private $server="DESKTOP-P992EA1\SW";
    private $usr="fkServer";
    private $psw="fkServer21";
    private $defaultdb="boilerplate";
    protected $appName = "BoilerPlate";

    protected function connectSqlSrv()
    {
        try{
            $dbCnx = new PDO("sqlsrv:Server=$this->server;Database=$this->defaultdb;ConnectionPooling=0", $this->usr, $this->psw);
            $dbCnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dbCnx->setAttribute(PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_UTF8);
            //$dbCnx->setAttribute(PDO::MYSQL_ATTR_FOUND_ROWS, true);
            $dbCnx->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            return $dbCnx;
        }
        catch(PDOException $e){
            echo $e;
            die();
            return null;
        }
    }

    protected function connectSqlSrvDb($db)
    {
        try{
            $dbCnx = new PDO("sqlsrv:Server=$this->server;Database=$db", $this->usr, $this->psw);
            $dbCnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dbCnx->setAttribute(PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_UTF8);
            //$dbCnx->setAttribute(PDO::MYSQL_ATTR_FOUND_ROWS, true);
            $dbCnx->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            return $dbCnx;
        }
        catch(PDOException $e){
            echo $e;
            die();
            return null;
        }
    }

    protected function ExecuteNoQuery($sth)
    {
        $r=array('data'=>false,
        'error'=>false,
        'r'=>'');
        try {   
            $sth->execute();     
            if ($r['r'] = $sth->rowCount()) {
                $r['data'] = true; 
            }
        } catch (PDOException $e) {
            $r['error']=true;
            $r['r'] = $e->getMessage();
        }
        return $r;
    }

    protected function ExecuteSelectAssoc($sth, $instance = null, $func = null, $name = null, $fkey = null) 
    {
        $r=array('data'=>false,
        'error'=>false,
        'r'=>array());
        try {           
            $sth->execute();
            while ($row=$sth->fetch()) {
                $r['data'] = true;
                $keys = array_keys($row);
                $tmp = array();
                foreach ($keys as $key) {
                    $tmp[$key] = trim($row[$key]);
                }
                if(isset($instance) && isset($func) && isset($name) && isset($fkey))
                    $tmp[$name] = $instance->$func($row[$fkey]);
                array_push($r['r'], $tmp);
            }
        } catch (PDOException $e) {
            $r['error']=true;
            $r['r'] = $e->getMessage();
        }
        return $r;
    }

    protected function ExecuteSelectArray($sth) 
    {
        $r=array('data'=>false,
        'error'=>false,
        'r'=>array());
        try {           
            $sth->execute();
            while ($row=$sth->fetch(PDO::FETCH_NUM)) {
                $r['data'] = true;
                $tmp = array();
                foreach ($row as $value) {
                    array_push($tmp, trim($value));
                }
                array_push($r['r'], $tmp);
            }
        } catch (PDOException $e) {
            $r['error']=true;
            $r['r'] = $e->getMessage();
        }
        return $r;
    }



}