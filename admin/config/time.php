<?php
header("Content-Type: application/json");
$files = $_POST['files'];
$retval = array("js"=>array(), "css"=>array());
if(isset($files['js']))
    $retval['js'] = get($files['js']);
if(isset($files['css']))
    $retval['css'] = get($files['css']);

echo json_encode($retval);

function getTime($file)
{
    $tmpFile = dirname(dirname(dirname(__FILE__)))."/".$file;
    if (file_exists($tmpFile)) {    
        return array("url"=>$file, "time"=>filemtime($tmpFile));
    } else {
        return array("url"=>$file);
    }
}

function get($bundle)
{
    $ret = array();
    foreach ($bundle as $value) {
        array_push($ret, getTime($value));
    }
    return $ret;
}