<?php
class Auth
{
    public static $DEFAULT_RESPONSE = '{"data":false,"error":true,"r":"Acceso Denegado"}';
    public static $REQUEST;
    public static $GRANT_ACCESS = false;
    public static $CAN_CREATE = false;
    public static $CAN_READ = false;
    public static $CAN_UPDATE = false;
    public static $CAN_DELETE = false;

    public static function caseAction($s)
    {
        return self::$REQUEST['action'] == $s && self::$GRANT_ACCESS;
    }

    public static function getAccess($permit)
    {
        session_start();
        $obj = new SessionManager();
        $permit = $obj->getPermit($permit);
        session_write_close();

        self::$CAN_CREATE = false;
        self::$CAN_READ = false;
        self::$CAN_UPDATE = false;
        self::$CAN_DELETE = false;

        if (self::$GRANT_ACCESS = $permit['data']) {
            self::$CAN_CREATE = $permit['r'][0]['C'];
            self::$CAN_READ = $permit['r'][0]['R'];
            self::$CAN_UPDATE = $permit['r'][0]['U'];
            self::$CAN_DELETE = $permit['r'][0]['D'];
        }
    }
}