<?php 
class SessionManager extends ConnectionManager
{
    public function Login($data)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare("SELECT user_tag, nombre, correo, usuario, psw, id_rol, active FROM usuarios WHERE usuario = :username");
        $sth->bindParam(':username', $data['user']);

        $r = $this->ExecuteSelectAssoc($sth);
        try {
            if (!$r['data']) {
                $error = 'Usuario incorrecto.';
                throw new Exception($error); 
            }

            $r['r'] = $r['r'][0];
            if (!(boolean)$r['r']['active']) {
                $error = 'Usuario desactivado.';
                throw new Exception($error); 
            }

            if (hash_equals(trim($r['r']['psw']), crypt($data['psw'], trim($r['r']['psw'])))) {
                $token = $this->generateTag(25);
                $_SESSION[$this->appName] = array();
                $_SESSION[$this->appName]['name'] = $r['r']['nombre'];
                $_SESSION[$this->appName]['access_token'] = $token;
                $_SESSION[$this->appName]['email'] = $r['r']['correo'];

                $sth = $cnx->prepare(
                    "INSERT INTO session_tokens(user_tag, token, last_login) 
                    VALUES(:tag, :token, GETDATE())"
                );
                $sth->bindParam(':token', $token);
                $sth->bindParam(':tag', $r['r']['user_tag']);
                $r = $this->ExecuteNoQuery($sth);
                if($r['data'])
                    $r['r'] = $this->getDefaultState($_SESSION[$this->appName]['access_token']);
            } else {
                $error = 'Contraseña incorrecta.';
                throw new Exception($error);
            }

            if (isset($data['remember']) && (boolean)$data['remember']) {
                setcookie($this->appName."_token", $token, time()+432000);
            }
        } catch(Exception $e){
            $r['data'] = false;
            $r['error'] = true;
            $r['r'] = $e->getMessage();
        }

        return json_encode($r);
    }

    public function logOut()
    {
        if (isset($_SESSION[$this->appName]) && isset($_COOKIE[$this->appName."_token"])) {
            $cnx = $this->connectSqlSrv();
            $sth = $cnx->prepare(
                "DELETE FROM session_tokens WHERE token = :token"
            );
            $sth->bindParam(':token', $_COOKIE[$this->appName."_token"]);
            $this->ExecuteNoQuery($sth);
            unset($_COOKIE[$this->appName."_token"]);
            setcookie($this->appName."_token", null, -1);
        } else if (isset($_COOKIE[$this->appName."_token"]) && $this->validToken($_COOKIE[$this->appName."_token"])) {
            return $this->cookieSession($_COOKIE[$this->appName."_token"]);
        } else {
            $_SESSION[$this->appName] = array();
            unset($_SESSION[$this->appName]);
        }
       
        return Auth::$DEFAULT_RESPONSE;
    }

    private function cookieSession($token)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "SELECT u.nombre, u.correo 
            FROM usuarios u INNER JOIN session_tokens s ON s.user_tag=u.user_tag
            WHERE s.token = :token"
        );
        $sth->bindParam(':token', $token);
        $r = $this->ExecuteSelectAssoc($sth);
        if ($r['data']) {
            $r['r'] = $r['r'][0];
            $_SESSION[$this->appName] = array();
            $_SESSION[$this->appName]['name'] = $r['r']['nombre'];
            $_SESSION[$this->appName]['access_token'] = $token;
            $_SESSION[$this->appName]['email'] = $r['r']['correo'];
            setcookie($this->appName."_token", $token, time()+432000);
        }
        return json_encode($r);
    }

    public function checkSession()
    {
        $retval = json_decode(Auth::$DEFAULT_RESPONSE, true);

        if (isset($_SESSION[$this->appName]) && isset($_SESSION[$this->appName]['access_token']) && $this->validToken($_SESSION[$this->appName]['access_token'])) {
            $retval['data'] = true;
            $retval['r'] = array(
                "name"=>$_SESSION[$this->appName]['name'],
                "email" =>$_SESSION[$this->appName]['email'],
                "permits" => $this->getPermits($_SESSION[$this->appName]['access_token']),
                "state" => $this->getDefaultState($_SESSION[$this->appName]['access_token'])
            );
        } else {
            
            $this->logOut();
        }
        return json_encode($retval);
    }

    private function validToken($token)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare("DELETE FROM session_tokens WHERE GETDATE() >= (CAST(last_login as DATETIME) + 5)");
        $this->ExecuteNoQuery($sth);
        $sth = $cnx->prepare("UPDATE session_tokens SET last_login=GETDATE() WHERE token = :token");
        $sth->bindParam(':token', $token);
        $r = $this->ExecuteNoQuery($sth);
        return $r['data'];
    }

    private function getPermits($token)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "SELECT u.id_rol FROM usuarios u 
            INNER JOIN session_tokens s ON s.user_tag=u.user_tag
            WHERE s.token=:token"
        );
        $sth->bindParam(":token", $token);  
        $r = $this->ExecuteSelectAssoc($sth);
        if ($r['data']) {
            $sth = $cnx->prepare(
                "SELECT m.alias, rm.C, rm.R, rm.U, rm.D FROM roles_menus rm
                INNER JOIN menus m ON m.id_menu=rm.id_menu
                WHERE rm.id_rol=:rol"
            );
            $sth->bindParam(":rol", $r['r'][0]['id_rol']);  
            $r = $this->ExecuteSelectAssoc($sth);
        } else {
            $r = array("r" => array()); 
        }
        return $r['r'];
    }

    public function getPermit($menu)
    {
        $token = $_SESSION[$this->appName]['access_token'];
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "SELECT u.id_rol FROM usuarios u 
            INNER JOIN session_tokens s ON s.user_tag=u.user_tag
            WHERE s.token=:token"
        );
        $sth->bindParam(":token", $token);  
        $r = $this->ExecuteSelectAssoc($sth);
        if ($r['data']) {
            $sth = $cnx->prepare(
                "SELECT m.alias, rm.C, rm.R, rm.U, rm.D FROM roles_menus rm
                INNER JOIN menus m ON m.id_menu=rm.id_menu
                WHERE rm.id_rol=:rol AND m.alias=:menu"
            );
            $sth->bindParam(":rol", $r['r'][0]['id_rol']);
            $sth->bindParam(":menu", $menu);  
            $r = $this->ExecuteSelectAssoc($sth);
        } else {
            $r = array("r" => array()); 
        }
        return $r;
    }

    private function getDefaultState($token)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "SELECT r.state FROM usuarios u 
            INNER JOIN roles r ON r.id_rol=u.id_rol
            INNER JOIN session_tokens s ON s.user_tag=u.user_tag
            WHERE s.token=:token"
        );
        $sth->bindParam(":token", $token);  
        $r = $this->ExecuteSelectAssoc($sth);
        return $r['r'];
    }

    private function generateTag($length = 5)
    {
        $buf = '';
        for ($i = 0; $i < $length; ++$i) {
            $buf .= chr(mt_rand(0, 255));
        }
        return strtoupper(bin2hex($buf));
    }
}