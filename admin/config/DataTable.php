<?php

abstract class DataTable
{
    protected function list_filter( $list, $args = array(), $operator = 'AND' )
    {
        if (! is_array($list)) {
            return array();
        }
    
        $util = new List_Util($list);
    
        return $util->filter($args, $operator);
    }

    protected function getDataTable($data, $request)
    {
        $datatable = ! empty($request) ? $request : array();
        $datatable = array_merge(array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable);
        
        $arrayfilter = isset($datatable[ 'query' ][ 'generalSearch' ]) && is_string($datatable[ 'query' ][ 'generalSearch' ]) ? preg_split('/\s+/', $datatable[ 'query' ][ 'generalSearch' ]) : array();
        foreach ($arrayfilter as $filter) {   
            if (! empty($filter)) {
                $data = array_filter(
                    $data, function ($a) use ($filter) {
                        $a =  (array)$a;
                        $notString = false;
                        foreach ($a as $key => $value) {
                            if (gettype($value) === "array") {
                                $txt = array();
                                foreach ($a[$key] as $v) {
                                    if (isset($v['name']))
                                        array_push($txt, $v['name']);
                                    else {
                                        echo "Property name must be set for array filter.\n";
                                        return;
                                    }
                                }
                                $notString = $notString || (boolean)preg_grep("/$filter/i", $txt);
                                unset($a[$key]);
                            }
                        }
                        $string = (boolean)preg_grep("/$filter/i", $a);
                        return $notString || $string;
                    }
                );
            }
        }
        
        if(isset($datatable['query' ][ 'generalSearch']))
            unset($datatable['query' ][ 'generalSearch']);
        
        $query = isset($datatable['query']) && is_array($datatable['query']) ? $datatable['query'] : null;
        if (is_array($query)) {
            $query = array_filter($query);
            foreach ($query as $key => $val) {
                if ($key!="action")
                    $data = $this->list_filter($data, array($key => $val));
            }
        }
        
        $sort  = ! empty($datatable[ 'sort' ][ 'sort' ]) ? $datatable[ 'sort' ][ 'sort' ] : 'asc';
        $field = ! empty($datatable[ 'sort' ][ 'field' ]) ? $datatable[ 'sort' ][ 'field' ] : 'RecordID';
        
        $meta    = array();
        $page    = ! empty($datatable[ 'pagination' ][ 'page' ]) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
        $perpage = ! empty($datatable[ 'pagination' ][ 'perpage' ]) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;
        
        $pages = 1;
        $total = count($data);
        
        if ($perpage > 0) {
            $pages  = ceil($total / $perpage);
            $page   = max($page, 1);
            $page   = min($page, $pages);
            $offset = ($page - 1) * $perpage;
            if ($offset < 0) {
                $offset = 0;
            }
            
            $data = array_slice($data, $offset, $perpage, true);
        }
        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );
        usort(
            $data, function ($a, $b) use ($sort, $field) {
                if (! isset($a[$field]) || ! isset($b[$field])) {
                    return false;
                }
                
                if ($sort === 'asc') {
                    return $a[$field] > $b[$field] ? true : false;
                }
        
                return $a[$field] < $b[$field] ? true : false;
            } 
        );
        
        $result = array(
            'meta' => $meta + array(
                    'sort'  => $sort,
                    'field' => $field,
                ),
            'data' => $data,
        );

        return json_encode($result, JSON_PRETTY_PRINT);
    }
}