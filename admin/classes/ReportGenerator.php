<?php

require_once '../../../vendor/autoload.php';
use Dompdf\Dompdf;

class ReportGenerator
{
    public function GeneratePdf($data, $opts = array())
    {
        $options = array("template" => "genericDT", "size" => "letter", "orientation" => "landscape");
        $options = array_replace_recursive($options, $opts['reportConfig']);
        $config =  array('extension' => '.html');
        $m = new Mustache_Engine(
            array(
                'template_class_prefix' => '__Reportes_',
                'cache' => dirname(__FILE__).'/tmp/cache/',
                'cache_file_mode' => 0666, 
                'loader' => new Mustache_Loader_FilesystemLoader(dirname(dirname(__FILE__))."\\reports\\", $config),
                'partials_loader' => new Mustache_Loader_FilesystemLoader(dirname(dirname(__FILE__)).'/reports/partials', $config),
                'escape' => function($value) {
                    return htmlspecialchars($value, ENT_COMPAT, 'UTF-8');
                }
            )
        );
        $dompdf = new Dompdf();
        $dompdf->set_base_path("../../../assets/vendors/");

        $tpl = $m->loadTemplate($options['template']);
        $rendered = $tpl->render($data);
        $dompdf->loadHtml($rendered);
        $dompdf->setPaper($options['size'], $options['orientation']);
        $dompdf->render();
        return base64_encode($dompdf->output());
    }

    public function parseData($data, $config, $helpers)
    {
        date_default_timezone_set("America/Phoenix");
        $headers = $config['columns'];
        $rows=array();
        foreach ($data as $row) {
            $aux = array();
            foreach ($row as $key=>$value) {
                if (array_search($key, array_column($headers, 'field')) !== false) {
                    if (isset($helpers) && isset($helpers[$key])) {
                        array_push($aux, $helpers[$key]($value));
                    } else {
                        array_push($aux, $value);
                    }
                }
            }
            array_push($rows, array_values($aux));
        }
        return array("headers" => $headers, "title"=>isset($config['title']) ? $config['title'] : "", "date"=>date('d/m/Y h:i:s a', time()), "rows"=>$rows);
    }
}