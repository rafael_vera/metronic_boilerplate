<?php 
class RolesManager extends ConnectionManager
{
    public function newRol($dt)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "INSERT INTO roles (rol, [state]) OUTPUT inserted.id_rol 
            VALUES (:rol, :state)"
        );
        $sth->bindParam(":rol", $dt['rol']);
        $sth->bindParam(":state", $dt['state']);
        $r = $this->ExecuteSelectAssoc($sth);
        $id = $r['r'][0]['id_rol'];
        foreach ($dt['permisos'] as $value) {
            if (isset($value['permit']) && (boolean)$value['permit']) {
                $sth = $cnx->prepare(
                    "INSERT INTO roles_menus (id_rol, id_menu, C, R, U, D) 
                    OUTPUT inserted.id_rol 
                    VALUES (:id_rol, :id_menu, :C, :R, :U, :D)"
                );
                $value['C'] = $value['C'] == "true" ? 1 : 0;
                $value['R'] = $value['R'] == "true" ? 1 : 0;
                $value['U'] = $value['U'] == "true" ? 1 : 0;
                $value['D'] = $value['D'] == "true" ? 1 : 0;
                $sth->bindParam(":id_rol", $id);
                $sth->bindParam(":id_menu", $value['id_menu']);
                $sth->bindParam(":C", $value['C']);
                $sth->bindParam(":R", $value['R']);
                $sth->bindParam(":U", $value['U']);
                $sth->bindParam(":D", $value['D']);
                $r = $this->ExecuteNoQuery($sth);
            }
        }
        return json_encode($r);
    }


    public function editRol($dt)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "UPDATE roles SET rol=:rol, [state]=:state
            WHERE id_rol=:id"
        );
        $sth->bindParam(":id", $dt['id_rol']);
        $sth->bindParam(":rol", $dt['rol']);
        $sth->bindParam(":state", $dt['state']);
        $r = $this->ExecuteNoQuery($sth);
        $sth = $cnx->prepare(
            "DELETE FROM roles_menus WHERE id_rol=:id"
        );
        $sth->bindParam(":id", $dt['id_rol']);
        $r = $this->ExecuteNoQuery($sth);
        foreach ($dt['permisos'] as $value) {
            if (isset($value['permit']) && (boolean)$value['permit']) {
                $sth = $cnx->prepare(
                    "INSERT INTO roles_menus (id_rol, id_menu, C, R, U, D) 
                    VALUES (:id_rol, :id_menu, :C, :R, :U, :D)"
                );
                $value['C'] = $value['C'] == "true" ? 1 : 0;
                $value['R'] = $value['R'] == "true" ? 1 : 0;
                $value['U'] = $value['U'] == "true" ? 1 : 0;
                $value['D'] = $value['D'] == "true" ? 1 : 0;
                $sth->bindParam(":id_rol", $dt['id_rol']);
                $sth->bindParam(":id_menu", $value['id_menu']);
                $sth->bindParam(":C", $value['C']);
                $sth->bindParam(":R", $value['R']);
                $sth->bindParam(":U", $value['U']);
                $sth->bindParam(":D", $value['D']);
                $r = $this->ExecuteNoQuery($sth);
            }
        }
        return json_encode($r);
    }

    public function setActiveRol($id, $val)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "UPDATE roles SET active=:val WHERE id_rol=:id"
        );
        $sth->bindParam(":id", $id);
        $sth->bindParam(":val", $val);
        $r = $this->ExecuteNoQuery($sth);

        return json_encode($r);
    }
    

    public function deleteRol($id)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "DELETE FROM roles WHERE id_rol=:id"
        );
        $sth->bindParam(":id", $id);
        $r = $this->ExecuteNoQuery($sth);
        $sth = $cnx->prepare(
            "DELETE FROM roles_menus WHERE id_rol=:id"
        );
        $sth->bindParam(":id", $id);
        $r = $this->ExecuteNoQuery($sth);

        return json_encode($r);
    }

    public function getRoles($request)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "SELECT id_rol, rol, active FROM roles"
        );
        $r = $this->ExecuteSelectAssoc($sth);
        foreach ($r['r'] as $key => $value) {
            $sth = $cnx->prepare("SELECT m.menu as name, r.C, r.R, r.U, r.D, r.id_menu FROM roles_menus as r INNER JOIN menus as m ON r.id_menu=m.id_menu WHERE r.id_rol=:rol");
            $sth->bindParam(":rol", $value['id_rol']);
            $aux = $this->ExecuteSelectAssoc($sth);
            $r['r'][$key]['permisos'] = $aux['r'];
        }
        return $this->getDataTable($r['r'], $request);
    }

    public function getRolesArray()
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "SELECT id_rol as id, rol as [text] FROM roles WHERE active=1"
        );
        $r = $this->ExecuteSelectAssoc($sth);
        return json_encode($r);
    }

    public function getRol($id)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "SELECT id_rol, rol, [state], active FROM roles WHERE id_rol=:id"
        );
        $sth->bindParam(":id", $id);
        $r = $this->ExecuteSelectAssoc($sth);
        foreach ($r['r'] as $key => $value) {
            $sth = $cnx->prepare(
                "SELECT m.menu as name, r.C, r.R, r.U, r.D, r.id_menu 
                FROM roles_menus as r 
                INNER JOIN menus as m ON r.id_menu=m.id_menu WHERE r.id_rol=:rol"
            );
            $sth->bindParam(":rol", $value['id_rol']);
            $aux = $this->ExecuteSelectAssoc($sth);
            $r['r'][$key]['permisos'] = $aux['r'];
        }
        $r['r'] = $r['r'][0];
        return json_encode($r);
    }
}