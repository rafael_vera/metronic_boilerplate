<?php 
class MenusManager extends ConnectionManager
{
    public function newMenu($menu)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "INSERT INTO menus (menu, alias, router, type, icon) 
            VALUES (:menu, :alias, :router, :type, :icon)"
        );
        $sth->bindParam(":menu", $menu['menu']);
        $sth->bindParam(":alias", $menu['alias']);
        $sth->bindParam(":router", $menu['router']);
        $sth->bindParam(":type", $menu['type']);
        $sth->bindParam(":icon", $menu['icon']);
        $r = $this->ExecuteNoQuery($sth);
        return json_encode($r);
    }

    public function updateMenu($menu)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "UPDATE menus SET menu=:menu, alias=:alias, router=:router, type=:type, icon=:icon
            WHERE id_menu=:id_menu"
        );
        $sth->bindParam(":menu", $menu['menu']);
        $sth->bindParam(":alias", $menu['alias']);
        $sth->bindParam(":router", $menu['router']);
        $sth->bindParam(":type", $menu['type']);
        $sth->bindParam(":icon", $menu['icon']);
        $sth->bindParam(":id_menu", $menu['id_menu']);
        $r = $this->ExecuteNoQuery($sth);
        return json_encode($r);
    }

    public function getMenusBuilder($root = -1)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "SELECT id_menu, menu, alias, router, type, root, icon FROM menus 
            WHERE root = :root ORDER BY priority"
        );
        $sth->bindParam(":root", $root);
        $sth->execute();
        $retval = $this->ExecuteSelectAssoc($sth, $this, 'getMenusBuilder', 'nodes', 'id_menu');

        $retval['data'] = $retval['r'] == array() ? false : true;
        if($root == -1)
            return json_encode($retval);
        else
            return $retval['r'];
    }

    public function updateMenuPosition($menu, $root=-1)
    {    
        $cnx = $this->connectSqlSrv();   
        foreach ($menu as $key => $value) {
            $sth = $cnx->prepare("UPDATE menus SET priority=:priority, root=:root, type=:type WHERE id_menu = :id");
            $sth->bindParam(":id", $value['id']);
            $sth->bindParam(":priority", $key);
            $sth->bindParam(":root", $root);
            $sth->bindParam(":type", $value['type']);
            if (isset($value['children']))
                $this->updateMenuPosition($value['children'], $value['id']);
            $retval = $this->ExecuteNoQuery($sth);
        }
        return json_encode($retval);
    }

    public function deleteMenu($id)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare("DELETE FROM menus WHERE id_menu = :id");
        $sth->bindParam(":id", $id);
        $retval = $this->ExecuteNoQuery($sth);
        return json_encode($retval);
    }
}