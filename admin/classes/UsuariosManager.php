<?php

class UsuariosManager extends ConnectionManager
{
    public function userExist($usuario)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare("SELECT usuario FROM usuarios WHERE usuario=:usuario");
        $sth->bindParam(':usuario', $usuario);
        $r = $this->ExecuteSelectArray($sth);
        return $r['data'] ? "false" : "true";
    }

    public function getUsuario($id)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "SELECT u.nombre, u.usuario, u.correo, u.id_rol FROM usuarios u
             WHERE u.user_tag=:id"
        );
        $sth->bindParam(':id', $id);
        $r = $this->ExecuteSelectAssoc($sth);
        $r['r'] = $r['r'][0];
        return json_encode($r);
    }

    public function getUsuarios($request)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "SELECT u.user_tag, u.nombre, u.usuario, u.correo, r.rol, u.active FROM usuarios u
            INNER JOIN roles r ON r.id_rol=u.id_rol"
        );
        $r = $this->ExecuteSelectAssoc($sth);
        if($r['data'])
            return $this->getDataTable($r['r'], $request);
        else
            return json_encode($r);
    }
    
    public function newUsuario($dt)
    {
        if ($this->userExist($dt['usuario'])) {
            $cnx = $this->connectSqlSrv();
            $sth = $cnx->prepare(
                "INSERT INTO usuarios (nombre, usuario, correo, psw, id_rol)
                VALUES (:nombre, :usuario, :correo, :psw, :id_rol)"
            );

            $cost = 10;
            $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
            $salt = sprintf("$2a$%02d$", $cost) . $salt;
            $hash = crypt($dt['psw'], $salt);

            $sth->bindParam(":nombre", $dt['nombre']);
            $sth->bindParam(":usuario", $dt['usuario']);
            $sth->bindParam(":correo", $dt['correo']);
            $sth->bindParam(":psw", $hash);
            $sth->bindParam(":id_rol", $dt['rol']);
            $r = $this->ExecuteNoQuery($sth);
        } else {
            $r['data'] = false;
            $r['error'] = true;
            $r['r'] = "Usuario ya registrado";
        }
        return json_encode($r);
    }
    
    public function deleteUsuario($id)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare(
            "DELETE FROM usuarios WHERE user_tag=:id"
        );
        $sth->bindParam(":id", $id);
        $r = $this->ExecuteNoQuery($sth);
        return json_encode($r);
    }
    
    public function updateUsuario($dt)
    {
        $cnx = $this->connectSqlSrv();
        if (isset($dt['psw'])) {
            $sth = $cnx->prepare(
                "UPDATE usuarios SET nombre=:nombre, usuario=:usuario, correo=:correo, id_rol=:rol, psw=:psw WHERE user_tag=:id"
            );
            $cost = 10;
            $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
            $salt = sprintf("$2a$%02d$", $cost) . $salt;
            $hash = crypt($dt['psw'], $salt);
            $sth->bindParam(":psw", $hash);
        } else {
            $sth = $cnx->prepare(
                "UPDATE usuarios SET nombre=:nombre, usuario=:usuario, correo=:correo, id_rol=:rol WHERE user_tag=:id"
            );
        }

        $sth->bindParam(":nombre", $dt['nombre']);
        $sth->bindParam(":usuario", $dt['usuario']);
        $sth->bindParam(":correo", $dt['correo']); 
        $sth->bindParam(":rol", $dt['rol']);
        $sth->bindParam(":id", $dt['user_tag']);
        $r = $this->ExecuteNoQuery($sth);

        return json_encode($r);
    }

    public function setActiveUsuario($id, $val)
    {
        $cnx = $this->connectSqlSrv();
        $sth = $cnx->prepare("UPDATE usuarios SET active=:val WHERE user_tag=:id");
        $sth->bindParam(':id', $id);
        $sth->bindParam(':val', $val);
        $r = $this->ExecuteNoQuery($sth);
        return json_encode($r);
    }

    public function randomPassword($len = 6)
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $len; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }
}